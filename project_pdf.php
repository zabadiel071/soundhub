<?php
	ob_start();
	require_once 'config.php';
	require_once ROOT_DIR.'\crud\project.db.php';
	require_once ROOT_DIR.'\crud\user_project.db.php';
	require_once ROOT_DIR.'\crud\user.db.php';
	require_once 'view.php';
	class PDF{

		public function getData($id_project){
			//Project data
			$db = new Project;
			$db->connectdb();
			$array['project'] = $db->readOne($id_project);
			//Files data
			$array['files'] = $db->files_detail($id_project);

			foreach ($array['files'] as $key => $value) {
				$array['files'][$key]['source'] = basename($value['source']);
			}
			$db=null;
			//Collaborators data
			$db = new User_Project;
			$db->connectdb();
			$array['collaborators'] = $db->collab_details($id_project);
			$db = null;

			return $array;
		}

		public function printPDF($id_project){
			$array = $this->getData($id_project);
			$pdfString = View::renderPDF($array,'project_detail.html');
		    // convert in PDF
		    require_once('vendor/autoload.php');
		    try
		    {
		        $html2pdf = new HTML2PDF('P','A4','es');
		        $html2pdf->writeHTML($pdfString);
		        $html2pdf->setDefaultFont('Arial');
		        ob_end_clean();
		        $html2pdf->Output('project_detail.pdf');
		    }
		    catch(HTML2PDF_exception $e) {
		        echo $e;
		        exit;
		    }
		}

	}

	$pdf = new PDF;

	switch ($_REQUEST['id_project']) {
		default:
			$pdf->printPDF($_REQUEST['id_project']);
			break;
	}
?>