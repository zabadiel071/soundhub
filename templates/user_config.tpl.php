{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<div class="row">
		<h1>{USER}</h1>
	</div>
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Actualizar datos personales</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<form class="form" action="user.php" method="post">
								<input type="hidden" name="action" value="updateData">
								<div class="form-group">
								    <label for="name">Nombre</label>
								    <input type="text" class="form-control datafield" name="name" value="{name}" required>
								</div>					
								<div class="form-group">
								    <label for="lastname">Apellido</label>
								    <input type="text" class="form-control datafield" name="lastname" value="{lastname}" required>
								</div>								
								<div class="form-group">
								    <label for="username">Nombre de usuario</label>
								    <input type="text" class="form-control datafield" name="username" value="{username}" required>
								</div>					
								<div class="form-group">
								    <label for="passwordCheck">Confirmación de contraseña</label>
								    <input type="password" class="form-control datafield" name="passwordCheck" required>
								</div>
								<button type="submit" class="btn btn-primary" name="submit" value="update">Listo</button>
							</form>								
						</div>
					</div>
				</div> 
			</div>
		</div> <!--Columna de datos personales-->
		<div class="col-md-6 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Cambiar contraseña</div>
				<div class="panel-body">
					<form class="form" action="user.php" method="post">
						<input type="hidden" name="action" value="changePassword">
						<div class="form-group">
						    <label for="password">Contraseña actual</label>
						    <input type="password" class="form-control datafield" name="password" required>
						</div>
						<div class="form-group">
						    <label for="newPassword">Nueva contraseña</label>
						    <input type="password" class="form-control datafield" name="newPassword" required>
						</div>
						<div class="form-group">
						    <label for="newPasswordCheck">Confirmar nueva contraseña</label>
						    <input type="password" class="form-control datafield" name="newPasswordCheck" required>
						</div>
						<button type="submit" class="btn btn-primary" name="submit" value="updatePassword">Listo</button>
					</form>
				</div>
			</div>
			<h4>¿Deseas abandonar soundhub?</h4>
			<form class="form" action="user.php" method="post">
				<input type="hidden" name="action" value="finish">
				<button type="submit" class="btn btn-primary" name="submit" value="finish">Salir de SH</button>
			</form>
		</div>
	</div>
</div> 
