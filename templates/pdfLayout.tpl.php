<style>
  h1{
    font-size: 36px;
  }
  h2{
    font-size: 24px;
  }
  ul{
  	list-style: none;
	font-size: 18px;
  }
  p{
  	font-size: 18px;
  }
  table{
  	border-style: solid;
  }
  table tr{
  	border-style: solid;
  }

  table thead{
  	font-size: 18px;
  }
  
  table tr td{
  	font-size: 14px;
  	border-style: solid;
  }
</style>
{content}