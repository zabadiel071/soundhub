<div class="container"  id="page-header">
	<div class="container">
		<div class="row">
			<h1>Audio</h1>
		</div>
	</div>
</div>

<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-xs-12">
				<form action="file.php" method="post" enctype="multipart/form-data">
					<div class="form-group">
						Selecciona archivo a subir
						<input type="file" name="fileToUpload" id="fileToUpload" class="form-control-file" required>	
					</div>
					<div class="form-group">
						<label for="name">Nombre del archivo</label>
						<input type="text" class="form-control datafield" name="name" required>
					</div>
					<div class="form-group">
						<label for="instrument">Instrumento</label>
						<select class="form-control" name="instrument" id="instrument">
						  {CONTENT}
						</select>
				    </div>
					<div class="form-group">
						Description
 						<textarea class="form-control" rows="3" name="description"></textarea>			
					</div>
					<button type="submit" class="btn btn-primary"  name="submit" value="submit">Submit</button>
				</form>
			</div>
		</div> 
	</div>
</div>
