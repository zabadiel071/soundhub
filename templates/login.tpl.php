<div class="container-fluid">
	<div id="container-login">
		<h1>Ingresa a SoundHub</h1>
		<form action="login.php" method="post">
			<input type="hidden" name="action" value="validate">
			<div class="form-group">
			    <label for="email">Email</label>
			    <input type="email" class="form-control datafield" name="email" required>
			</div>					
			<div class="form-group">
			    <label for="password">Contraseña</label>
			    <input type="password" class="form-control datafield" name="password" required>
			</div>				
			<button type="submit" class="btn btn-primary" name="submit" value="login">Listo</button>
		</form>
		<a href="login.php?action=recovery">Olvidaste tu contraseña?</a>
	</div>
</div>