<div class="jumbotron">
    <div class="container" id="jumbotron-container">
      <h1>{JUMBO_HEADER}</h1>
        <p>{JUMBO_TEXT}</p>
        <p>
        <a class="btn btn-primary btn-lg" href="#" role="button">{JUMBO_LEARNMORE}</a></p>
      </div>
</div>
<div class="container-fluid" id="featurette-container">
  <div class="row">
    <h1 id="features-title">{FEATURES_TITLE}</h1>
  </div>
  <div class="featurette">
  <div class="container" id="globe">
      <div class="row">
        <img src="img/globe.jpg">
      </div>
      <div class="row">
        <h2>{FEATURE1_TITLE}</h2>    
      </div>
      <div class="row">
        <p>{FEATURE1_TEXT}</p>
      </div>
  </div>
</div>
<div class="featurette">
  <div class="container" id="">
      <div class="row">
        <img src="img/music-gear.jpg" id="music-gear">
      </div>
      <div class="row">
        <h2>{FEATURE2_TITLE}</h2>    
      </div>
      <div class="row">
        <p>{FEATURE2_TEXT}</p>
      </div>
  </div>
</div>
<div class="featurette">
  <div class="container" id="">
      <div class="row">
        <img src="img/piggy-bank.png" id="piggy-bank">
      </div>
      <div class="row">
        <h2>{FEATURE3_TITLE}</h2>    
      </div>
      <div class="row">
        <p>{FEATURE3_TEXT}</p>
      </div>
  </div>
</div>
<div class="container" id="center-btn-container">
  <a class="btn btn-primary" href="login.php?action=register" role="button">{BTN_START_NOW}</a>
</div>
</div>