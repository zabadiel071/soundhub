<div class="container-fluid">
	<div id="container-login">
		<h1>Recuperar contraseña</h1>
		<form action="login.php" method="post">
			<input type="hidden" name="action" value="reset">
			<div class="form-group">
			    <label for="email">Email</label>
			    <input type="email" class="form-control datafield" name="email" required>
			</div>				
			<button type="submit" class="btn btn-primary" name="submit" value="login">Listo</button>
		</form>
	</div>
</div>