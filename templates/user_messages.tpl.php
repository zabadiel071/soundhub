{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<div class="row">
		<h1>{USER}</h1>
	</div>
	<div class="row">
		<div class="col-md-8">
			<ul class="nav nav-tabs">
				<li class="active">
					<a data-toggle="tab" href="#enviados">Enviados</a>
				<li>
				<li>
					<a data-toggle="tab" href="#recibidos">Recibidos</a>
				<li>
				<li>
					<a data-toggle="tab" href="#send_form">Nuevo</a>
				</li>
			</ul>
			{CONTENT}

		</div>
		<div class="col-md-4">
			<div class="container-fluid">
				<h3>Stats</h3>
			</div>
		</div>
	</div>
</div> 