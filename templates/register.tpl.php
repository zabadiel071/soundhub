<div class="container"  id="page-header">
	<div class="container">
		<div class="row">
			<h1>Registrate</h1>
		</div>
		<div class="row">
			<h2>Estas a unos pasos</h2>
		</div>
	</div>
</div>
<script>
function validateForm() {
	var password = document.forms["register"]["password"].value;
	var password_check = document.forms["register"]["password_check"].value;

	if(password != password_check){
		alert("Verifica la contraseña");
		return false;
	}
}
</script>
<div class="container">
	<div class="container">
		<div class="row">
			<div class="col-md-6 column">
				<form name="register" action="login.php"  onsubmit="return validateForm()" method="post">
					<div class="form-group">
				 		<label for="username">Nombre de usuario</label>
						<input type="text" class="form-control datafield" name="username" required>
					</div>	
					<div class="form-group">
				 		<label for="name">Nombre</label>
						<input type="text" class="form-control datafield" name="name" required>
					</div>			
					<div class="form-group">
						<label for="lastname">Apellido</label>
						<input type="text" class="form-control datafield" name="lastname" required>
					</div>
					<div class="form-group">
					    <label for="email">Email</label>
					    <input type="email" class="form-control datafield" name="email" required>
					</div>					
					<div class="form-group">
					    <label for="password">Contraseña</label>
					    <input type="password" class="form-control datafield" name="password" required>
					</div>
					<div class="form-group">
					    <label for="password">Confirmar contraseña</label>
					    <input type="password" class="form-control datafield" name="password_check" required>
					</div>					
					<input type="hidden" name="action" value="create" >
  					<button type="submit" class="btn btn-primary" name="submit" value="register">Listo</button>
				</form>
			</div>
			<div class="col-md-6">
				<img src="img/ah-kosmos.jpg">
			</div>
		</div>
	</div>
</div>
