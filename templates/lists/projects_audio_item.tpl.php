<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">
			<a href="">{name}</a>
		</h3>
	</div>
	<div class="panel-body audio-item-container">
		<div class="row">
			<div class="col-md-10 col-xs-12 audio-item-right">
				<br>
				<audio class="audio-player" controls><source src="uploads\project_{id_project}{name}" type="audio/mpeg"> Error</audio>
				<br>
					{description}
				<br>	
			</div>
			<div class="col-md-2 col-xs-12">
				<br>
				<!--Delete,Update-->
				<form method="POST" action="project.php" class="form-inline">
					<input type="hidden" name="id_file" value="{id_file}">
					<div class="form-group">
						<button type="submit" name="action" value="delete" class="file-action">
							<span class="glyphicon glyphicon-trash"></span>		
						</button>		
						<button type="submit" name="action" value="updateFile" class="file-action">
							<span class="glyphicon glyphicon-pencil"></span>		
						</button>		
					</div>
				</form>	<!--Delete,update-->
			</div>
		</div>		
	</div>
</div>