<div class="tab-content">
	<div id="enviados" class="tab-pane fade in active">
		{SENTS}	
	</div>
	<div id="recibidos" class="tab-pane fade">
		{RECEIVES}
	</div>
	<div id="send_form" class="tab-pane fade">
		<div class="row">
			<div class="col-md-6 col-xs-8">
				<form action="user.php" method="post">
					<input type="hidden" name="action" value="send_message">
					<input type="hidden" name="id_sender" value="{id_sender}">
					<div class="form-group">
					    <label for="username">Nombre de usuario</label>
					    <input type="text" class="form-control datafield" name="username" required>
					</div>		
					<div class="form-group">
						<label for="body">Mensaje</label>
						<textarea class="form-control" rows="5" name="body"></textarea>
					</div>
					<button type="submit" class="btn btn-primary" name="submit" value="nuevoMensaje">Listo</button>
				</form>	
			</div>		
		</div>
	</div>
</div>