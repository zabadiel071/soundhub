<div class="panel panel-default">
	<div class="panel-heading">Proyectos</div>
	<div class="panel-body">
		Dale un vistazo a los proyectos que otros usuarios han subido a Soundhub
	</div>
	<table class="table">
		<tr>
			<th>Nombre</th>
			<th>Información</th>
			<th>Inicio</th>
			<th></th>
		</tr>
		{projects}
	</table>	
</div>