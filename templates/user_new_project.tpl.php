<div class="container-fluid" id="wrapper">
	<div class="row">
		<h3>Nuevo Proyecto</h3>	
	</div>
	<div class="row">
		<div class="col-md-4">
			<form method="POST" action="user_new_project.php">
				<div class="form-group">
			 		<label for="title">Titulo</label>
					<input type="text" class="form-control datafield" name="title" required>
				</div>	
				<div class="form-group">
			 		<label for="info">Descripción</label>
					<textarea class="form-control" name="info" rows="5" style="resize: none;" required></textarea>
				</div>	
				<div class="form-group">
					<label class="radio-inline">
						<input type="radio" name="privacity" id="input1" required value="public" >Publico 
					</label>
					<label class="radio-inline">
						<input type="radio" name="privacity" id="input2" value="private">Privado 
					</label>
				</div>
				<button type="submit" class="btn btn-primary" name="submit" value="new_project">Listo</button>				
			</form>
		</div>
	</div>
</div>