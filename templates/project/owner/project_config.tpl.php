{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Configuración de proyecto</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<form method="POST" action="project.php">
							<input type="hidden" name="action" value="updateData">
							<input type="hidden" name="id_project" value="{id_project}">
								<div class="form-group">
							 		<label for="title">Titulo</label>
									<input type="text" class="form-control datafield" name="title" value="{title}" required>
								</div>	
								<div class="form-group">
							 		<label for="info">Descripción</label>
									<textarea class="form-control" name="info" rows="5" style="resize: none;" required>{info}</textarea>
								</div>	
								<div class="form-group">
									<label class="radio-inline">
										<input type="radio" name="public" id="input1" required value="1" {checkPublic}>Publico 
									</label>
									<label class="radio-inline">
										<input type="radio" name="public" id="input2" value="0" {checkPrivate}>Privado 
									</label>
								</div>
								<button type="submit" class="btn btn-primary" name="submit" value="updateData">Listo</button>				
							</form>								
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>
</div> 