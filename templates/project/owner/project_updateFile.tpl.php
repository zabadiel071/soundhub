{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Configuración de archivo</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<form method="POST" action="project.php">
							<input type="hidden" name="action" value="updateDataFile">
							<input type="hidden" name="id_project" value="{id_project}">
							<input type="hidden" name="id_file" value="{id_file}">
							<input type="hidden" name="id_format" value="{id_format}">
							<input type="hidden" name="id_instrument" value="{id_instrument}">
								<div class="form-group">
							 		<label for="name">Nombre</label>
									<input type="text" class="form-control datafield" name="name" value="{name}" required>
								</div>	
								<div class="form-group">
							 		<label for="description">Descripción</label>
									<textarea class="form-control" name="description" rows="5" style="resize: none;" required>{description}</textarea>
								</div>	
								<button type="submit" class="btn btn-primary" name="submit" value="updateDataFile">Listo</button>				
							</form>								
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>
</div> 