{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<ul class="nav nav-pills">
			  <li role="presentation"><a href="project.php?action=new_file">Añadir archivo</a></li>
			  <li role="presentation"><a href="project.php?action=history">Ver historial</a></li>
			</ul>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-8 col-xs-12 list-container">
			<!--CONTENT-->
			  <div class="tab-pane" id="audios">
				{CONTENT}
		      </div>
		</div>
		<div class="col-md-4 col-xs-12">
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">{PROJECT}</h3>
			  </div>
			  <div class="panel-body">
			   	{info}
			  </div>
			</div>							
		</div>
	</div>
</div> 