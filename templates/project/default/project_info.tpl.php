{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">{title}</h3>
			  </div>
			  <div class="panel-body">
				<ul class="list-group">
					<li class="list-group-item">Titulo : {title}</li>
					<li class="list-group-item">Publico/Privado : {public}</li>
					<li class="list-group-item">Fecha inicio : {date_begin}</li>
				</ul>
				{info}
			  </div>
			</div>	
		</div>
	</div>
</div> 