{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<br>
	<div class="row">
		<div class="col-md-8 col-xs-12 list-container">
			<!--CONTENT-->
			  <div class="tab-pane" id="audios">
				{CONTENT}
		      </div>
		</div>
		<div class="col-md-4 col-xs-12">
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">{PROJECT}</h3>
			  </div>
			  <div class="panel-body">
			   	Esta es una descripción que muestra aspectos importantes del proyecto
			  </div>
			</div>							
		</div>
	</div>
</div> 