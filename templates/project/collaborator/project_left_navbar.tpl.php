<nav class="navbar" id="left-navbar">
	<ul id="navbar-list">						
                <li id="tab-li">
                        <a href="project.php?action=info">
                                <h5>Info</h5>
                                <span class="glyphicon glyphicon-list-alt"></span>
                        </a>
                </li>
		<li id="tab-li" class="active">
			<a href="project.php?action=audios">
				<h5>Archivos</h5>
				<span class="glyphicon glyphicon-music"></span>
			</a>
		</li>									
                <li id="tab-li">
                	<a href="project.php?action=collaborators">
                		<h5>Colaboradores</h5>
                		<span class="glyphicon glyphicon-heart"></span>
                	</a>
                </li>
	</ul>
</nav>