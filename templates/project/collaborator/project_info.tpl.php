{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">{title}</h3>
			  </div>
			  <div class="panel-body">
				<ul class="list-group">
					<li class="list-group-item">Titulo : {title}</li>
					<li class="list-group-item">Publico/Privado : {public}</li>
					<li class="list-group-item">Fecha inicio : {date_begin}</li>
				</ul>
				{info}
			  </div>
			</div>	
		</div>
	</div>
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<form class="form" action="project_pdf.php" method="get">
				<input type="hidden" name="id_project" value="{id_project}">
				<div class="form-group">
					<button type="submit" class="btn btn-default">
						PDF de la información del proyecto
					</button>
				</div>
			</form>
		</div>				
	</div>
</div> 