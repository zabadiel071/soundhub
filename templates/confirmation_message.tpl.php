<div class="container"  id="page-header">
	<div class="container">
		<div class="row">
			<h1>Listo</h1>
		</div>
		<div class="row">
			<h2>Tu cuenta ha sido creada, ahora puedes iniciar sesión</h2>
				<div class="col-md-6 col-xs-12">
					<form action="login.php" method="post">
						<input type="hidden" name="action" value="validate">
						<div class="form-group">
						    <label for="email">Email</label>
						    <input type="email" class="form-control datafield" name="email" required>
						</div>					
						<div class="form-group">
						    <label for="password">Contraseña</label>
						    <input type="password" class="form-control datafield" name="password" required>
						</div>				
						<button type="submit" class="btn btn-primary" name="submit" value="login">Listo</button>
					</form>	
				</div>
			</div>
		</div>
	</div>
</div>
