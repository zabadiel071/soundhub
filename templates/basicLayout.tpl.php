<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{TITLE}</title>
    <script src="https://code.jquery.com/jquery-3.2.0.min.js" integrity="
    sha256-JAW99MJVpJBGcbzEuXk4Az05s/XyDdBomFqNlM3ic+I=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
    {navbar}
    {content}
</body>
<footer id="footer">
  {footer}
</footer>
</footer>
<script type="text/javascript">
  var list = document.getElementsByTagName("h5");
  var mediaquery = window.matchMedia("(max-width: 768px)");
    if (mediaquery.matches) {
        for (var i = list.length - 1; i >= 0; i--) {
          list[i].parentNode.removeChild(list[i]);
        }
        document.getElementsByClassName('tab-content')[0].style.marginLeft = 0;
    }
</script>
</html>