<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="user.php?action=projects">SH</a>
    </div>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="project.php?action=read"><p id="text-link">Explora</p></a></li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder={NAV_SEARCH} size="50">
        </div>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <div class="btn-group" id="user-options">
            <button type="button" id="user-add" class="user-options-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="glyphicon glyphicon-plus"></span>
            </button>
              <ul class="dropdown-menu">
                <li><a href="user.php?action=new_project">Nuevo proyecto</a></li>
                <li><a href="user.php?action=joinTo">Unirse a proyecto</a></li>
              </ul>
          </div>
        </li> <!--'Add' options-->
        <li>
          <div class="btn-group" id="user-options">
            <button type="button" class="user-options-btn" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             <span class="caret"></span>
            </button>
              <ul class="dropdown-menu">
                <li><a href="user.php?action=config">Configuración</a></li>
                <li><a href="login.php?action=logout">Cerrar sesión</a></li>
                <li><a href="user.php?action=help">Ayuda</a></li>
              </ul>
          </div>  
        </li> <!--'User' options-->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>