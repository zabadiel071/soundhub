
<div class="container-fluid" id="wrapper">
	<div class="row">
		<h1>{USER}</h1>
	</div>
	<div class="row">
		<div class="col-md-8">
			<form class="form" action="user.php" method="post">
			<h1>Estas seguro que deseas abandonar Soundhub?</h1>
				<input type="hidden" name="action" value="confirmFinish">
				<div class="form-group">
				    <label for="passwordCheck">Confirmación de contraseña</label>
				    <input type="password" class="form-control datafield" name="passwordCheck" required>
				</div>
				<button type="submit" class="btn btn-primary" name="submit" value="confirmFinish">
					Si, estoy seguro
				</button>
			</form>
		</div>
	</div>
</div> 