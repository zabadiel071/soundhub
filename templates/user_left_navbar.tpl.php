 <nav class="navbar" id="left-navbar">
	<ul id="navbar-list">						
		<li id="tab-li" class="active">
			<a href="user.php?action=projects">
				<h5>Proyectos</h5>
				<span class="glyphicon glyphicon-list"></span>
			</a>
		</li>
                <li id="tab-li">
                	<a href="user.php?action=messages">
                		<h5>Mensajes</h5>
                		<span class="glyphicon glyphicon-envelope"></span>
                	</a>
                </li>	

                <li id="tab-li">
                        <a href="user.php?action=collaborations">
                                <h5>Colaboraciones</h5>
                                <span class="glyphicon glyphicon-heart"></span>
                        </a>
                </li>
                <li id="tab-li">
                	<a href="user.php?action=config">
                		<h5>Configuración</h5>
                		<span class="glyphicon glyphicon-cog"></span>
                	</a>
                </li>
	</ul>
</nav>