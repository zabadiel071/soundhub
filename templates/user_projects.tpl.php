{LEFT-NAVBAR}
<div class="container-fluid" id="wrapper">
	<div class="row">
		<h1>{USER}</h1>
	</div>
	<div class="row">
		<div class="col-md-8">
		  <div class="tab-pane" id="projects">
			{CONTENT}
		  </div>
		</div>
		<div class="col-md-4">
			<div class="container-fluid">
				<h3>Stats</h3>
			</div>
		</div>
	</div>
</div> 