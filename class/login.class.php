<?php
	require_once CLASSPATH.'database.class.php';

	class Login extends Database{
		
		function __construct(){
			$this->connectdb();
		}
		/*
		 *
		 */
		function resetPassword($email){

		}

		/**
		 *	Retorna si una contraseña es la correcta para un usuario
		 */
		function validate($email,$password){
			$sql = "SELECT password FROM user WHERE email=:email";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':email', $email);
			$statement -> Execute();
			$data = $statement -> FetchAll(PDO::FETCH_ASSOC);
			return password_verify($password,$data[0]['password']); 
		}

		function logout(){
			session_destroy();
		}
	}

  ?>