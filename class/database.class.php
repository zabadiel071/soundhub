<?php
/**
 *  Conexión a la base de datos
 *  @author Zabdiel zabadiel071@gmail.com
 *  @version 0 
 */
class Database{	
	/**
	 * Variables
	 */
	
	var $bd = null;
	
	/**
	 * Comienza el PDO para realizar operaciones con base de datos
	 * Utiliza las variables globales DSN,DBUSER,DBPASS
	 */
	function connectdb(){
		try {
			$this->bd = new PDO(DSN,DBUSER,DBPASS);
		} catch (PDOException $e) {
		    print "¡Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}


	/**
	 *Convertir una consulta a un array
	 *@param $sql : consulta realizada
	 *@return $data : Array con los resultados de la consulta
	 */
	function query($sql){
		$statement = $this->bd->prepare($sql);
		$statement->Execute();
		return $statement->FetchAll(PDO::FETCH_ASSOC);
	}
	
	/**
	 * Realiza una sentencia sql
	 * @param sql : sentencia que se va a realizar
	 * @return filas : numero de filas afectadas
	 */
	function exec($sql){
		$rows_affected = $this->bd->exec($sql);
		return $rows_affected;
	}

	/**
	 *	Ejecuta una sentencia SQL y retorna un array asociativo
	 *	@param $query: Sentencia sql a realizar
	 */
	function fetch($query){
 		$statement = $this->bd->prepare($query);
 		$statement->execute();
 		$data = $statement->fetchAll(PDO::FETCH_ASSOC);
 		return $data;
	}

	/**
	 *	Recupera todos los registros que coincidan con un numero de parámetros dados por un array	
	 *	@param $prepared_query : sentencia preparada sql ex. 'SELECT * FROM '
	 *  @param $params : arreglo con los parámetros a recuperar
	 *	@param $search : permite realizar búsqueda por patrones, SELECT usando 'LIKE'
	 */
	function fetchAll($prepared_query,$params=array(),$search=false){
		$where='';
		$i=0;
		if (count($params>0)) {
			foreach ($params as $key => $value) {
				if($search){
					$where.=$key." like :".$key;
				}else{
					$where.=$key.'=:'.$key;					
				}
				if($i<count($params)-1){
					$where.=' and ';
				}
				$i++;
			}
		}
		if($where!=' '){
			$prepared_query=$prepared_query.' where '.$where;
		}
		$statement = $this->bd->prepare($prepared_query);
 		foreach ($params as $key => $value) {
 			if ($search) {
 				$statement->bindValue(':'.$key,'%'.$value.'%',PDO::PARAM_STR);	
 			}else{
 				$statement->bindValue(':'.$key,$value,PDO::PARAM_STR);	
 			}
 		}
 		$statement->Execute();
 		$data = $statement->FetchAll(PDO::FETCH_ASSOC);
 		return $data;		
	}

	/**
	 *	Valida los roles de los usuarios
	 */
	function access($role){
		if (isset($_SESSION['logged'])) {
			if ($_SESSION['logged']) {
				$flag = false;
				foreach ($_SESSION['roles'] as $rol) {
					foreach ($rol as $key => $value) {
						if ($role == $value) {
							$flag = true;
							break;
						}
					}
				}
				if (!$flag) {
					die("No tiene acceso a la página");
				}
			}else{
				die("No has iniciado sesión");
			}
		}else{
			die("Acceso denegado");
		}
	}

	function combo($array,$id=null){
		$sql = 'SELECT '.$array[0].','.$array[1].' FROM '.$array[1].' ORDER BY '.$array[1].' asc ';
		$datos = array();
		$datos = $this->fetch($sql);
		echo '<select name = "data['.$array[0].']">';
		echo '<option value="">Selecciona</option>';
		foreach ($datos as $key => $value) {
			$selected = '';
			if ($id == $value[$array[0]]) {
				$selected = 'selected';
			}
			echo '<option value = "'.$value[$array[0]].'"'.$selected.'>'.$value[$array[1]].'</option>';
		}
		echo '</select>';
	}

	function userCombo($array,$id=null,$name = null){
		$sql = 'SELECT '.$array[0].','.$array[2].' FROM '.$array[1].' ORDER BY '.$array[2].' asc ';
		$datos = array();
		$datos = $this->fetch($sql);
		if (!is_null($name)) {
			echo '<select name = "data['.$name.']">';
		}else{
			echo '<select name = "data['.$array[0].']">';
		}
		
		echo '<option value="">Selecciona</option>';
		foreach ($datos as $key => $value) {
			$selected = '';
			if ($id == $value[$array[0]]) {
				$selected = 'selected';
			}
			echo '<option value = "'.$value[$array[0]].'"'.$selected.'>'.$value[$array[2]].'</option>';
		}
		echo '</select>';
	}
	/**
	 * Solo usarse en crud
	 * @param  [type] $template [description]
	 * @param  [type] $data     [description]
	 * @param  [type] $action   [description]
	 * @param  [type] $id       [description]
	 * @return [type]           [description]
	 */
	function render($template,$data,$action=null,$id=null){
		include($_SERVER['DOCUMENT_ROOT'].'/soundhub/crud/templatescrud/header.php');
		include($_SERVER['DOCUMENT_ROOT'].'/soundhub/crud/templatescrud/'.$template);
		include($_SERVER['DOCUMENT_ROOT'].'/soundhub/crud/templatescrud/footer.php');
	}
}
?>