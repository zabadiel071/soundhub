<?php
/**
 * 	Toda acción que se requiera realizar sobre una plantilla
 */

class Template{
	/**
	 *	assignedValues: Valores que la plantilla va a obtener ['{KEY}'][VALOR]
	 * 	stringTemplate: Representa la plantilla como si fuera una cadena
	 */
	var $assignedValues = array();
	var $stringTemplate;

	/**
	 *	Constructor del objeto que maneja la plantilla
	 *	@param $_path : ruta de la plantilla html
	 */
	function __construct($_path = ''){
		if(!empty($_path)){
			if(file_exists($_path)){
				$this->stringTemplate .= file_get_contents($_path);
			}else{
				echo "<b>Template error </b> : File inclusion error";
				die();
			}
		}
	}

	/**
	 *	Asigna un valor a la variable en la plantilla que se va a reemplazar
	 *	@param $_searchString  : Key a reemplazar
	 *	@param $_replaceString : valor que se va a tener en donde estaba {KEY}
	 */
	function set($_searchString,$_replaceString){
		if(!empty($_searchString)){
			$this -> assignedValues[$_searchString] = $_replaceString;
		}
	}

	/**
	 *	Asigna el valor contenido en otro archivo template, no hace ningún reemplazo en la plantilla
	 *	@param $_searchString : key a reemplazar
	 *	@param $_path: ruta del template a incorporar
	 */
	function setFileContent($_searchString,$_path){
		if (!empty($_path)) {
			if (file_exists($_path)) {
				$fileTemplating = new Template($_path);
				$this -> set($_searchString,$fileTemplating->output());
				$fileTemplating = null;
			}	
		}
	}

	/*
	 *	Salida de la plantilla con los valores establecidos y remplazados
	 */
	function output(){			
		if (count($this->assignedValues) > 0) {
			foreach ($this->assignedValues as $key => $value) {
				$this->stringTemplate = str_replace('{'.$key.'}', $value, $this->stringTemplate);					
			}
		}
		return $this->stringTemplate;
	}

	static public function merge($templates,$separator = "\n"){
		$output = "";

		foreach ($templates as $template) {
			$content = (get_class($template) !== "Template")
				? "Error, incorrect type--expected template"
				: $template->output();
			$output.=$content.$separator;
		}
		return $output;
	}

}
?>