<?php
require 'template.class.php';

/**
*	Template cargada con componentes 
*	Clase utilizada para cargar una página y añadirle componentes
*/

class Page extends Template{

	public $langArray = null;

	/**
	 * Creación de una página
	 * @param [string] $navbar      [template navbar]
	 * @param [string] $content     [template content]
	 * @param [string] $footer      [template footer]
	 * @param [string] $language    [clave del lenguaje, ej. 'en' , 'es' , si no está definido, la 
	 *                               plantilla se copiará tal como está]
	 * @param [string] $basicLayout [plantilla 'esqueleto' de la pagina]
	 */
	function __construct(
						$navbar=null,
						$content=null,
						$footer=null,
						$language = null,
						$basicLayout = TEMPLATES_PATH.'basicLayout.tpl.php'
						)
	{
		parent::__construct($basicLayout);
		$this->addNavbar($navbar);
		$this->addContent($content);
		$this->addFooter($footer);
		$this->defineLanguage($language); 
	}
		
	/**
	 * Set navbar
	 * @param [string] $_path [navbar.tpl path]
	 */
	function addNavbar($_path=null){
		if (is_null($_path)) {
			$this->set('navbar','');
		}else{
			$this->setFileContent('navbar',$_path);			
		}
	}

	/**
	 * Set content
	 * @param [string] $_path [content.tpl path , debe contener todo el cuerpo html de la página]
	 */
	function addContent($_path=null){
		if (is_null($_path)) {
			$this->set('content','');
		}else{
			$this->setFileContent('content',$_path);			
		}
	}

	/**
	 * Set footer
	 * @param [string] $_path [footer.tpl path]
	 */
	function addFooter($_path=null){
		if (is_null($_path)) {
			$this->set('footer','');
		}else{
			$this->setFileContent('footer',$_path);			
		}
	}

	/**
	 * Realiza todas las sustituciones necesarias para que la pagina tenga todo el texto y contenido 
	 * requerido. 
	 * Revisar si futuramente puede añadir datos de MODEL. Solo cargando otro arreglo
	 * @return [type] [description]
	 * IMPORTANTE: CORREGIR PARA QUE SEA DINÁMICA LA CARGA USANDO LA PÁGINA REFERENCIA COMO INDICE
	 */
	function render(){
		$aux  = $this->langArray['INDEX'];
		foreach ($aux as $key => $value) {
			$this->set($key,$value);
		}
	}

	/**
	 * Carga el arreglo del lenguaje si se define en la declaración
	 * Si no se recibe una clave de lenguaje, el arreglo será null, y se carga el template por defecto
	 * @param  [string] $languageKey [Clave del lenguaje, ejemplo es = 'Español']
	 * @return [array]	Implicito
	 */
	function defineLanguage($languageKey = null){
		if (isset($languageKey)){
			switch ($languageKey){
			    case 'es':
			        require(LANGPATH.'lang.es.php');
			        $this->langArray = $lang;
			        break;       
			    default:
			        require(LANGPATH.'lang.en.php');
			        $this->langArray = $lang;
			        break;
		  }
		}
	 }
}
?>