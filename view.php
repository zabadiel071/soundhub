<?php
	require_once 'config.php';
	require_once CLASSPATH.'page.class.php';
	class View{

		/**
		 * Render
		 * @param  [type] $main    [description]
		 * @param  string $navbar  [description]
		 * @param  [type] $data    [description]
		 * @param  [type] $context [description]
		 * @return [type]          [description]
		 */
		public static function render($main,$navbar = 'default',$data=null,$context=null){
	      $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

	      $page  = new Page(TEMPLATES_PATH.'navbar-'.$navbar.'.tpl.php',
	                        TEMPLATES_PATH.$main.'.tpl.php',
	                        TEMPLATES_PATH.'footer.tpl.php',
	                        $language); 
	      if (isset($data)) {
	      	foreach ($data as $key => $value) {
	      		$page->set($key,$value);
	      	}
	      }
	      if (!is_null($context)) {
			$page->setFileContent('LEFT-NAVBAR',TEMPLATES_PATH.$context.'_left_navbar.tpl.php');
	      }
	      $page->render();
	      echo $page->output();  
    	}

    	/**
		 * Obtiene elementos de listados
		 * @param  [type] $classItem [tipo de item {projects | messages | favs | config }]
		 * @param  [type] $data      [array de datos]
		 * @return [type]            [description]
		 */
		static function getItems($classItem,$data){
			$templates = array();
			foreach ($data as $node) {
				$item = new Template(TEMPLATES_PATH."/pdf/".$classItem."_list_item.tpl");
				foreach ($node as $key => $value) {
			 		 $item->set($key,$value);
				}
				$templates[] = $item;
			}
			return $templates;
		}

    	public static function renderPDF($array,$file,$return = true){
    		$page  = new Page('',TEMPLATES_PATH.'pdf/'.$file,'','',TEMPLATES_PATH.'pdfLayout.tpl.php'); 
    		$page->set('navbar','');
    		$page->set('footer','');
    		foreach ($array['project'] as $key => $value) {
    			$page->set($key,$value);
    		}
    		$files = Template::merge(
    								View::getItems('files',$array['files'])
    								);
    		$list = new Template(TEMPLATES_PATH.'/pdf/files_list.tpl');
    		$list->set('fileData',$files);
    		$page->set('files_data',$list->output());
    		$collaborators = Template::merge(
    								View::getItems('collaborator',$array['collaborators'])
    								);
    		$list = new Template(TEMPLATES_PATH.'/pdf/collaborator_list.tpl');
    		$list->set('collaborator_data',$collaborators);
    		$page->set('collaborators_data',$list->output());
    		$page->render();

   			return $page->output();
    	}
	}
  ?>