-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-06-2017 a las 10:08:47
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `soundhub`
--
CREATE DATABASE IF NOT EXISTS `soundhub` DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;
USE `soundhub`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `file`
--

DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id_file` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `id_format` int(11) NOT NULL,
  `name` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `source` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `id_instrument` int(11) DEFAULT NULL,
  `date_upload` date NOT NULL,
  `description` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `file`
--

INSERT INTO `file` (`id_file`, `id_project`, `id_format`, `name`, `source`, `id_instrument`, `date_upload`, `description`) VALUES
(6, 2, 2, 'lightsDown.wav', 'C:\\xampp\\htdocs\\soundhub\\uploads\\project_2lightsDown.wav', 5, '2017-06-07', 'Voice sample'),
(7, 2, 2, 'Hi hat.wav', 'C:\\xampp\\htdocs\\soundhub\\uploads\\project_2Hi hat.wav', 6, '2017-06-07', 'Hi hat'),
(8, 2, 2, 'bass.wav', 'C:\\xampp\\htdocs\\soundhub\\uploads\\project_2bass.wav', 4, '2017-06-07', 'Bass'),
(10, 2, 2, 'snare.wav', 'C:\\xampp\\htdocs\\soundhub\\uploads\\project_2snare.wav', 6, '2017-06-07', 'snare'),
(11, 3, 2, 'Loop-sample.wav', 'C:\\xampp\\htdocs\\soundhub\\uploads\\project_3Loop-sample.wav', 6, '2017-06-07', 'Drums'),
(12, 2, 2, 'Beatbox.wav', 'C:\\xampp\\htdocs\\soundhub\\uploads\\project_2Beatbox.wav', 5, '2017-06-08', 'Sample of NY');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `format`
--

DROP TABLE IF EXISTS `format`;
CREATE TABLE `format` (
  `id_format` int(11) NOT NULL,
  `format` varchar(200) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `format`
--

INSERT INTO `format` (`id_format`, `format`) VALUES
(1, 'mp3'),
(2, 'wav'),
(4, 'flac');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instrument`
--

DROP TABLE IF EXISTS `instrument`;
CREATE TABLE `instrument` (
  `id_instrument` int(11) NOT NULL,
  `instrument` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `instrument`
--

INSERT INTO `instrument` (`id_instrument`, `instrument`) VALUES
(1, 'Guitar'),
(2, 'Piano'),
(3, 'Synth'),
(4, 'Other'),
(5, 'samples'),
(6, 'Drums');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id_message` int(11) NOT NULL,
  `id_sender` int(11) DEFAULT NULL,
  `id_receiver` int(11) DEFAULT NULL,
  `body` text COLLATE latin1_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `message`
--

INSERT INTO `message` (`id_message`, `id_sender`, `id_receiver`, `body`) VALUES
(1, 1, 2, 'Hola, me invitas a tu proyecto?'),
(2, 2, 1, 'Claro, te agrego como colaborador :)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id_permission` int(11) NOT NULL,
  `permission` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `permission`
--

INSERT INTO `permission` (`id_permission`, `permission`) VALUES
(1, 'Insertar'),
(2, 'Consultar'),
(3, 'Eliminar'),
(4, 'Actualizar'),
(5, 'Incorporar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id_project` int(11) NOT NULL,
  `id_creator` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE latin1_spanish_ci NOT NULL,
  `public` tinyint(1) DEFAULT NULL,
  `info` text COLLATE latin1_spanish_ci NOT NULL,
  `date_begin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `project`
--

INSERT INTO `project` (`id_project`, `id_creator`, `title`, `public`, `info`, `date_begin`) VALUES
(1, 1, 'Astral experience', 1, 'Proyecto de psytrance', '2017-06-07'),
(2, 2, 'Natural Causes', 1, 'Emancipator cover, i want to produce in a professional studio', '2017-06-07'),
(3, 1, 'Saxon shore', 1, 'Post-Rock experiment', '2017-06-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `role` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `id_role_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id_role`, `role`, `id_role_type`) VALUES
(1, 'common', NULL),
(2, 'admin', NULL),
(3, 'owner', NULL),
(4, 'collaborator', NULL),
(6, 'producer', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id_role_permission` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_permission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `role_permission`
--

INSERT INTO `role_permission` (`id_role_permission`, `id_role`, `id_permission`) VALUES
(2, 2, 2),
(3, 2, 3),
(4, 2, 1),
(5, 2, 4),
(6, 3, 5),
(7, 3, 1),
(8, 3, 3),
(9, 3, 2),
(10, 3, 4),
(11, 1, 1),
(12, 4, 4),
(13, 4, 2),
(14, 4, 3),
(15, 4, 5),
(16, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_type`
--

DROP TABLE IF EXISTS `role_type`;
CREATE TABLE `role_type` (
  `id_role_type` int(11) NOT NULL,
  `role_type` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `role_type`
--

INSERT INTO `role_type` (`id_role_type`, `role_type`) VALUES
(1, 'Project'),
(2, 'System'),
(16, 'External');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(254) COLLATE latin1_spanish_ci NOT NULL,
  `name` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `lastname` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `activation_hash` varchar(32) COLLATE latin1_spanish_ci NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `name`, `lastname`, `password`, `activation_hash`, `active`) VALUES
(1, 'Zabadiel071', 'zabadiel071@gmail.com', 'Zabdiel', 'GarcÃ­a', '$2y$10$X1yeTWZFurlWjliBVPZmouj8D8Jq48FicX1pG.ZIcPJwfRhouUDYO', '7ef605fc8dba5425d6965fbd4c8fbe1f', 1),
(2, 'zab', '14030620@itcelaya.edu.mx', 'Zabdiel', 'Garcia', '$2y$10$fJ1xw2OcPT.TPAEx4RhYlOsLV9CeYD4UveedhcRmtIyDC//FH6WKi', 'ce78d1da254c0843eb23951ae077ff5f', 1),
(6, 'Foo', 'test@mail.com', 'Testing', 'Doe', '$2y$10$.6UeapzI4M7hShALxMI.Ne4qQajzg1OPkJO1dMkoIuz2klBliNAOa', 'ebd9629fc3ae5e9f6611e2ee05a31cef', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_project`
--

DROP TABLE IF EXISTS `user_project`;
CREATE TABLE `user_project` (
  `id_user_project` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `user_project`
--

INSERT INTO `user_project` (`id_user_project`, `id_project`, `id_user`, `id_role`) VALUES
(1, 1, 1, 3),
(2, 1, 2, 4),
(3, 2, 1, 4),
(4, 2, 2, 3),
(5, 3, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id_user_role` int(11) NOT NULL,
  `id_role` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `user_role`
--

INSERT INTO `user_role` (`id_user_role`, `id_role`, `id_user`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id_file`),
  ADD KEY `id_type_content` (`id_format`),
  ADD KEY `id_instrument` (`id_instrument`),
  ADD KEY `file_ibfk_1` (`id_project`);

--
-- Indices de la tabla `format`
--
ALTER TABLE `format`
  ADD PRIMARY KEY (`id_format`);

--
-- Indices de la tabla `instrument`
--
ALTER TABLE `instrument`
  ADD PRIMARY KEY (`id_instrument`);

--
-- Indices de la tabla `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id_message`),
  ADD KEY `message_ibfk_1` (`id_sender`),
  ADD KEY `message_ibfk_2` (`id_receiver`);

--
-- Indices de la tabla `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id_permission`);

--
-- Indices de la tabla `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id_project`),
  ADD KEY `id_creator` (`id_creator`,`title`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`),
  ADD KEY `id_role_type` (`id_role_type`);

--
-- Indices de la tabla `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id_role_permission`),
  ADD KEY `id_permission` (`id_permission`),
  ADD KEY `id_role` (`id_role`);

--
-- Indices de la tabla `role_type`
--
ALTER TABLE `role_type`
  ADD PRIMARY KEY (`id_role_type`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`,`username`,`email`),
  ADD UNIQUE KEY `username` (`username`,`email`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indices de la tabla `user_project`
--
ALTER TABLE `user_project`
  ADD PRIMARY KEY (`id_user_project`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_role` (`id_role`),
  ADD KEY `id_project` (`id_project`,`id_user`);

--
-- Indices de la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id_user_role`),
  ADD KEY `id_role` (`id_role`),
  ADD KEY `user_role_ibfk_2` (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `file`
--
ALTER TABLE `file`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `format`
--
ALTER TABLE `format`
  MODIFY `id_format` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `instrument`
--
ALTER TABLE `instrument`
  MODIFY `id_instrument` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `message`
--
ALTER TABLE `message`
  MODIFY `id_message` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `permission`
--
ALTER TABLE `permission`
  MODIFY `id_permission` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `project`
--
ALTER TABLE `project`
  MODIFY `id_project` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id_role_permission` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `role_type`
--
ALTER TABLE `role_type`
  MODIFY `id_role_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `user_project`
--
ALTER TABLE `user_project`
  MODIFY `id_user_project` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id_user_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `file_ibfk_2` FOREIGN KEY (`id_format`) REFERENCES `format` (`id_format`),
  ADD CONSTRAINT `file_ibfk_3` FOREIGN KEY (`id_instrument`) REFERENCES `instrument` (`id_instrument`);

--
-- Filtros para la tabla `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`id_sender`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`id_receiver`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_creator`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `role`
--
ALTER TABLE `role`
  ADD CONSTRAINT `role_ibfk_1` FOREIGN KEY (`id_role_type`) REFERENCES `role_type` (`id_role_type`);

--
-- Filtros para la tabla `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`id_permission`) REFERENCES `permission` (`id_permission`),
  ADD CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`);

--
-- Filtros para la tabla `user_project`
--
ALTER TABLE `user_project`
  ADD CONSTRAINT `user_project_ibfk_1` FOREIGN KEY (`id_project`) REFERENCES `project` (`id_project`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_project_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `user_project_ibfk_3` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`);

--
-- Filtros para la tabla `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`),
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
