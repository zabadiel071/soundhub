var list = document.getElementsByTagName("h5");
var mediaquery = window.matchMedia("(max-width: 768px)");
if (mediaquery.matches) {
    for (var i = list.length - 1; i >= 0; i--) {
      list[i].parentNode.removeChild(list[i]);
    }
    document.getElementsByClassName('tab-content')[0].style.marginLeft = 0;
}