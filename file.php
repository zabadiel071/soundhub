<?php
	require_once 'config.php';
	require_once ROOT_DIR.'/crud/file.db.php';
	require_once ROOT_DIR.'\class\page.class.php';

	/**
	* 
	*/
	class File_control extends File{
	
		var $conflicto;

		/**
		 * Inicialización de las variables requeridas
		 */
		function __construct(){
			$this->conflicto['user-default'] = (isset($_SESSION['user']['email'])) ? 
					'user' : 'default';	
		}

		public function new(){
			$page_final = $this->factory('new');
			$page_final->set("CONTENT",$this->selectItems());
			$page_final->render();
			echo $page_final->output();
		}

		/**
		 * Crea los elementos del select instrumentos
		 * @return Array con los items
		 */
		function selectItems(){
			$instruments = $this->instruments();
		  	//Cargamos el item de cada proyecto
		  	foreach ($instruments as $project) {
		  		$item = new Template(TEMPLATES_PATH."/lists/instrument_select_options.tpl.php");	
			    foreach ($project as $key => $value) {
			      $item->set($key,$value);
			    }
			    $instrumentTemplates[] = $item;
		  	}
		  	return Template::merge($instrumentTemplates);
		}


		/**
		 * Renderiza de acuerdo a una accion determinada
		 * @param  String $action pagina principal a escribir en la ruta
		 * @return new Page 	  Objeto Pagina del que se obtiene un output
		 */
		function factory($action = null){
			$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
			$page_final = new Page(TEMPLATES_PATH.'navbar-'.$this->conflicto['user-default'].'.tpl.php',
				                    TEMPLATES_PATH.'file_'.$action.'.tpl.php',
				                    TEMPLATES_PATH.'footer.tpl.php',
				                    $language); 
			//$page_final->set("PROJECT", $_SESSION['current_project']['title']);
			$page_final->setFileContent('LEFT-NAVBAR',TEMPLATES_PATH.'project_left_navbar.tpl.php');
			return $page_final;
		}
	}

	$web = new File_control();
	$web->connectdb();

	if (isset($_GET['action'])) {
		$action = $_GET['action'];
		switch ($action) {
		case 'new':
			$web->new();
			break;
		default:
			break;
		}
	}

	if(isset($_POST['submit'])){
		$data = array();
		$dir = ROOT_DIR."\uploads\\";
		$format = pathinfo($_FILES['fileToUpload']['name'],PATHINFO_EXTENSION);
		$data['name'] = $_POST['name'].'.'.$format;
		$data['source']  = $dir.basename("project_".$_SESSION['current_project']['id_project'].$data['name']);
		$uploadOk = 1;
		//Si el achivo es de audio
		if( strpos($_FILES['fileToUpload']['type'],'audio/') !== false ){
			$uploadOk = 1;
		}else{
			echo '<script>alert("El archivo no es de audio");</script>';
			$uploadOk = 0;
		}
		//Si ya existe el archivo
		if( file_exists($data['source'])){
			echo '<script>alert("El archivo ya existe");</script>';
			$uploadOk = 0;
		}
		// //Solo algunos formatos
		if($format != "mp3" && $format != "wav" && $format != "ogg"){
			echo '<script>alert("Formato no soportado");</script>';
			$uploadOk = 0;
		}else{
			$data['id_format'] = $web->getFormat($format);
		}
		if ($uploadOk == 0) {
			echo '<script>alert("Tu archivo no se pudo subir");</script>';
		}else{
			//Todo está en orden, momento de subir el archivo
			if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $data['source'])) {
				echo '<script>alert("Tu archivo ha sido subido");</script>';
				//Se crea un registro en la base de datos
				$data['id_project'] = $_SESSION['current_project']['id_project'];
				$data['id_instrument'] = $_POST['instrument'];
				$data['description'] = $_POST['description'];						

				$web->create($data);
				header("Location: project.php?action=audios");
			}else{
				echo '<script>alert("Error del servidor al intentar subir tu archivo")';
			}
		}
	}
?>