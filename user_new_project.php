<?php
  ob_start();
  require 'config.php';
  require ROOT_DIR.'\class\page.class.php';
  require ROOT_DIR.'\crud\user.db.php';
  require ROOT_DIR.'\crud\project.db.php';
  require ROOT_DIR.'\crud\user_project.db.php';


  if (isset($_POST['submit'])) {
      $data = [
        "id_creator" => $_SESSION['user']['id_user'],
        "title" => $_POST['title'],
        "info" => $_POST['info'],
        "public" => $_POST['privacity'] == 'public' ? 1 : 0
      ];
      $bd = new Project();
      $bd->connectdb();
      $bd->create($data);
      // id_project del proyecto recién creado, usa id_user y title
      // id_role de owner
      $id_project = $bd->getID($_SESSION['user']['id_user'],$_POST['title']); 
      $bd = null;

      $data = null;
      $data = [
        'id_user' => $_SESSION['user']['id_user'],
        'id_project' => $id_project,
        'id_role' => 3
      ];

      $bd = new User_project;
      $bd -> connectdb();
      $bd -> create($data);

      header("Location: user.php?action=projects");

      ob_end_flush();
   }
?>