<?php
	require_once 'config.php';
	require ROOT_DIR.'\crud\user.db.php';
	require_once ROOT_DIR.'\crud\project.db.php';
	require_once ROOT_DIR.'\crud\message.db.php';
	require_once 'class\login.class.php';
	require ROOT_DIR.'\class\page.class.php';	
	require_once 'view.php';

	/**
	* 
	*/
	class User_control extends User{

		var $tempRoute = 'user/';
		/**
		 * Primer inicio de sesión, activación de cuenta
		 * @param  [array] $data [datos para inicio de sesión]
		 * @return [type]       [description]
		 */
		function verify($data){
			$this->activation($data,1);
			header('Location: confirmation.php?action=activation');
		}

		/**
		 * Carga la sección de projects del usuario
		 */
		function projects(){
			$db = new Project;
			$db->connectdb();
			$user_projects = $db->userProjects($_SESSION['user']['id_user']);
			foreach ($user_projects as $key => $value) {
					$user_projects[$key]['public'] = 
						($user_projects[$key]['public'] == 1)
					 		? 'Público' : 'Privado';
			}
			$contents = Template::merge( 
									$this->getItems('user_projects',$user_projects)
									);
			$list = new Template(TEMPLATES_PATH.'/lists/user_projects_list.tpl.php');
			$list->set("projects",$contents);
			$data = array(
				'CONTENT' => $list->output(),
				'USER' => $_SESSION['user']['username'],
				'id_user' => $_SESSION['user']['id_user']);
			View::render('user_projects','user',$data,'user');
		}

		function favs(){
			View::render('user_favs','user',null,'user');
		}

		function messages(){
			//Datos
			$db = new Message;
			$db->connectdb();
			$user_sents = $db->sent($_SESSION['user']['id_user']);
			$user_receives = $db->received($_SESSION['user']['id_user']);
			//Elementos html 
			$sents = Template::merge($this->getItems('user_sents',$user_sents));
			$receives = Template::merge($this->getItems('user_receives',$user_receives));
			//Listados html
			$list = new Template(TEMPLATES_PATH.'/lists/user_messages_list.tpl.php');
			$list->set("SENTS",$sents);
			$list->set("RECEIVES",$receives);
			//Paso a documento final
			$data = array(
				'USER' => $_SESSION['user']['username'],
				'CONTENT' => $list->output(),
				'id_sender' => $_SESSION['user']['id_user']
				);
			View::render('user_messages','user',$data,'user');
		}

		/**
		 *	Escribe un mensaje al usuario identificado con username
		 * 	@param $array => id_sender (usuario actual)
		 *					 username (id_receptor) //Buscar id
		 *					 body 	   (texto del mensaje)
		 */
		function writeMessage($array){
			$data['id_sender'] = $_SESSION['user']['id_user'];
			$data['body'] = $array['body'];
			$bd = new User;
			$bd->connectdb();
			$data['id_receiver'] = $bd->getID(null,$array['username']);
			$bd = null;

			$bd = new Message;
			$bd->connectdb();
			$bd->write($data);
			header("Location: user.php?action=messages");
		}

		function config(){
			$data = array(
					'USER' => $_SESSION['user']['username'],
					'username' => $_SESSION['user']['username'],
					'name' => $_SESSION['user']['name'],
					'lastname' => $_SESSION['user']['lastname'],
				);
			View::render('user_config','user',$data,'user');
		}

		/*
		
		 */
		function updateData($array){
			$sql = 'UPDATE user set name=:name,lastname=:lastname,username=:username WHERE id_user=:id_user';
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_user",$_SESSION['user']['id_user']);
			$statement->bindParam(":name",$array['name']);
			$statement->bindParam(":lastname",$array['lastname']);
			$statement->bindParam(":username",$array['username']);
			$statement->Execute();
			$_SESSION['user']['username'] = $array['username'];
			$_SESSION['user']['name'] = $array['name'];
			$_SESSION['user']['lastname'] = $array['lastname'];
			echo "<script>alert('Los datos se guardaron exitosamente')</script>";
			$this->projects();
		}

		function changePassword($email,$newPassword){
			$hash = password_hash($newPassword,PASSWORD_DEFAULT);
			$sql = 'UPDATE user set password=:password WHERE email=:email';
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":password",$hash);
			$statement->bindParam(":email",$email);
			$statement->Execute();
			echo "<script>alert('Los datos se guardaron exitosamente')</script>";
			$this->projects();
		}

		function deleteAccount($id_user){
			$sql = 'DELETE FROM user WHERE id_user=:id_user';
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_user",$id_user);
			$statement->Execute();
			echo "<script>alert('Se ha borrado tu cuenta')</script>";
			session_destroy();
			header("Location: index.php");
		}

		function collaborations(){
			$collaborations = $this->getCollaborations($_SESSION['user']['id_user']);
			$contents = Template::merge( 
									$this->getItems('user_collaborations',$collaborations)
									);
			$list = new Template(TEMPLATES_PATH.'/lists/user_collaborations_list.tpl.php');
			$list->set("collaborations",$contents);
			$data = array(
				'CONTENT' => $list->output(),
				'USER' => $_SESSION['user']['username'],
				'id_user' => $_SESSION['user']['id_user']);
			View::render('user_collaborations','user',$data,'user');
		}

		/**
		 * Obtiene elementos de listados
		 * @param  [type] $classItem [tipo de item {projects | messages | favs | config }]
		 * @param  [type] $data      [array de datos]
		 * @return [type]            [description]
		 */
		function getItems($classItem,$data){
			$templates = array();
			foreach ($data as $node) {
				$item = new Template(TEMPLATES_PATH."/lists/".$classItem."_item.tpl.php");
				foreach ($node as $key => $value) {
			 		 $item->set($key,$value);
				}
				$templates[] = $item;
			}
			return $templates;
		}

		function newProject(){
			View::render('user_new_project','user');
		}
	}

	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
	$web = new User_control;
	$web->connectdb();
	if ($action !=='verify') {
		$web->access('common');
	}
	
	$login = new Login;
	$login->connectdb();

	switch ($action) {
		case 'new_project':
			$web->newProject();
			break;
		case 'logout':
			$web->logout();
			break;
		case 'verify':
				$web->verify($_REQUEST);
			break;
		case 'projects':
				$web->projects();
			break;
		case 'collaborations':
				$web->collaborations();
			break;
		case 'favs':
				$web->favs();
			break;
		case 'messages':
				$web->messages();
			break;
		case 'config':
			$web->config();
			break;	
		case 'send_message':
			$web->writeMessage($_POST);
			break;
		case 'updateData':
			if (isset($_POST['passwordCheck'])) {
				//Validar la contraseña
				if ($login->validate($_SESSION['user']['email'],$_POST['passwordCheck'])) {
					$web->updateData($_POST);
				}else{
					echo "<script>alert('La contraseña no es valida')</script>";
					$web->config();
				}
			}else{
				echo "<script>alert('No se confirmó la contraseña')</script>";
			}	
		case 'changePassword':
			if (isset($_POST['password']) &&
				isset($_POST['newPassword']) &&
				isset($_POST['newPasswordCheck'])) {
				if ($login->validate($_SESSION['user']['email'],$_POST['password'])) {
					$web->changePassword($_SESSION['user']['email'],$_POST['newPassword']);
				}
			}else{
				echo "<script>alert('No se llenaron los campos necesarios')</script>";
			}
			break;	
		case 'finish':
			$data = array(
					'USER' => $_SESSION['user']['username']
				);
			View::render('user_final_chance','user',$data);
			break;
		case 'confirmFinish':
			if (isset($_POST['passwordCheck'])) {
				//Validar la contraseña
				if ($login->validate($_SESSION['user']['email'],$_POST['passwordCheck'])) {
					$web->deleteAccount($_SESSION['user']['id_user']);
				}else{
					echo "<script>alert('La contraseña no es valida')</script>";
						$data = array(
							'USER' => $_SESSION['user']['username']
						);
					View::render('user_final_chance','user',$data);	
				}
			}else{
				echo "<script>alert('No se confirmó la contraseña')</script>";
			}
			break;
	}

	
?>