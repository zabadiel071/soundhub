<?php
	/**
	* 
	*/
	class Response{

		/**
		 * Los mensajes que se envían
		 * @param  $action 		  [acción que la petición intenta realizar]
		 * @param  [type] $status [description]
		 * @return [array] $output  array con los resultados y el estado http 
		 */
		public static function log($action,$status){
			if($status){
				http_response_code(200);
				$output['status'] = 200;
				$output['message'] = "Se realizo $action con exito";
			}else{
				http_response_code(404);
				$output['status'] = 404; 
				$output['message'] = "No se pudo realizar $action";		
			}
			return $output;
		}

		/**
		 * Cuando falta un parámetro en la función
		 * @param  [type] $value [description]
		 * @return [array] $output array con los resultados y el estado http 
		 */
		public static function log_missedValue($value){
			http_response_code(404);
			$output['status'] = 404; 
			$output['message'] = "No se especificó el valor de $value";		
			return $output;
		}

		
	}
?>
