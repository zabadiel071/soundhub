<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once ROOT_DIR.'\crud\project.db.php';
	require_once ROOT_DIR.'\api\response.php';

	header('Content-Type: application/json');
	$method = $_SERVER['REQUEST_METHOD'];

	$web = new Project;
	$web->connectdb();

	$cadena = file_get_contents('php://input');
	$data = parse_json($cadena);

	//Conjunto de outputs, requerido para inserciones multiples
	$response = array();

	switch ($method) {
		//Insertar
		case 'POST':		
			foreach ($data as $key=>$value) {
				$statement = $web->create($value);
				$result = $statement->rowCount()>0;
				array_push($response,Response::log('insercion',$result));
			}
			break;
		//Actualizar
		case 'PUT':
			if(isset($_REQUEST['id'])){
				foreach ($data as $key => $value) {
					$statement = $web->update($_REQUEST['id'],$value);
					$result = $statement->rowCount()>0;
					array_push($response,Response::log('actualizacion',$result));
				}
			//$statement = $web->update($_REQUEST['id'],$json_obj);	
			}else{
				$response = Response::log_missedValue('id');
			}			
			break;
		//Borrar
		case 'DELETE':
			if (isset($_REQUEST['id'])) {
				$statement = $web->delete($_REQUEST['id']);
				$response = null;
				$response = Response::log('eliminacion',$statement->rowCount()>0);
			}else{
				$response = Response::log_missedValue('id');
			}
			break;
		//Mostrar
		default:
			if (isset($_REQUEST['id'])) {
				$response = $web->readOne($_REQUEST['id']);
			}else{
				$response = $web->read();	
			}
			
			break;
	}
	echo json_encode($response);

	/**
 	* La información que se recibe en el cuerpo de la petición (en JSON) se convierte
 	* a un array [$data] en el que se guardan como arreglos los objetos JSON
 	* @var [type]
 	*/
	function parse_json($cadena){
		$data = array();
		if ($cadena !== '') {
			$json_obj = json_decode($cadena);	
				foreach ($json_obj as $key=>$value){
					$nuevo = array(
							'id_creator'=>$value->id_creator,
							'title'=>$value->title,
							'public'=>$value->public,
							'info'=>$value->info
						); 				
					array_push($data, $nuevo);
				}		
		}else{
			$data = null;
		}
		return $data;
	}
?>