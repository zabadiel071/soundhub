Format
[{"format":"flac"}]

Instrument
[{"instrument":"bass"}]

Message
[{"id_sender":11,"id_receiver":1,"body":"Quiero participar en tu proyecto, puedo?"}]

Permission
[{"permission":"Clonar"}]

Project
[{"id_creator":1,"title":"soundhubForall","public":1,"info":"servicioWeb"}]

Role
[{"id_creator":1,"title":"soundhubForall","public":1,"info":"servicioWeb"}]

Role_Type
[{"role_type":"Application"}]

//Varias tablas
User_role
[{"role":"producer","username":"zab"}]

User_project
[{"title":"MaybeSheWill","username":"zab","role":"collaborator"}]

Role_permission
[{"role":"producer","permission":"consultar"}]
