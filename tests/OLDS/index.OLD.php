<?php
  /**
   * Recursos para hacer manejo de las plantillas
   *  i) Cargar la clase que maneja las plantillas
   *  ii) definir la ruta donde se encuentran las plantillas html
   */
  require('configuration.php');
  require_once('class/template.class.php');

  /**
   *
   */
  $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
  switch ($language) {
    case 'es':
        require('language/lang.es.php');
        render($lang);
        break;       
    default:
        require('language/lang.en.php');
        render($lang);
        break;
  }

  function render($lang){
    $basicLayout = new Template(TEMPLATES_PATH.'basicLayout.tpl');
    $basicLayout->set('title',$lang['TITLE']);

    /*
     *  Contenido del layout
     */
    $navbar = new Template(TEMPLATES_PATH.'navbar-default.tpl');
      $navbar->set('ABOUT',$lang['NAV_ABOUT']);
      $navbar->set('EXPLORE',$lang['NAV_EXPLORE']);
      $navbar->set('SEARCH',$lang['NAV_SEARCH_TEXT']);
      $navbar->set('REGISTER',$lang['NAV_REGISTER']);
      $navbar->set('LOGIN',$lang['NAV_LOGIN']);

    $content = new Template(TEMPLATES_PATH.'index.tpl');
      $content->set('JUMBO_HEADER',$lang['JUMBO_HEADER']);
      $content->set('JUMBO_TEXT',$lang['JUMBO_TEXT']);
      $content->set('JUMBO_LEARNMORE',$lang['JUMBO_LEARNMORE']);
      
      $content->set('FEATURES_TITLE',$lang['FEATURES_TITLE']);
        //feature1
      $content->set('FEATURE1_TITLE',$lang['FEATURE1_TITLE']);
      $content->set('FEATURE1_TEXT',$lang['FEATURE1_TEXT']);
      //feature2
      $content->set('FEATURE2_TITLE',$lang['FEATURE2_TITLE']);
      $content->set('FEATURE2_TEXT',$lang['FEATURE2_TEXT']);
      //feature3
      $content->set('FEATURE3_TITLE',$lang['FEATURE3_TITLE']);
      $content->set('FEATURE3_TEXT',$lang['FEATURE3_TEXT']);
      //Start now
      $content->set('BTN_START_NOW',$lang['BTN_START_NOW']);

    $footer = new Template(TEMPLATES_PATH.'footer.tpl');

    /*
     *  Añadiendo componentes al layout principal
     */
    $basicLayout->set('navbar',$navbar->output());
    $basicLayout->set('content',$content->output());
    $basicLayout->set('footer',$footer->output());
    
    //Impresion de la página
    echo $basicLayout->output();
  }
  ?>