<?php
/**
 * Crea el renderizado de una página utilizando arrays con los archivos a renderizar
 */

require('configuration.php');
require_once('class/template.class.php');
class Page{

	/**
	 * Arreglo dinámico con la traducción de la página
	 * @var $langArray
	 * 
	 */
	public $langArray = null;

	/**
	 * Define una instancia de una página, recibe el lenguaje de la página
	 * @param string(2) $lang : Clave del lenguaje, para traducciones
	 * @param string $title : titulo de la pagina
	 */
	function __construct($title,$languageKey = null){
		$this->defineLanguage($languageKey);
		
	}

	public function render(){
		
	}

	/**
	 * Carga el arreglo del lenguaje si se define en la declaración
	 * Si no se recibe una clave de lenguaje, el arreglo será null, y se carga el template por defecto
	 * @param  [string] $languageKey [Clave del lenguaje, ejemplo es = 'Español']
	 * @return [array]	Implicito
	 */
	function defineLanguage($languageKey){
		if (isset($languageKey)){
			switch ($languageKey){
			    case 'es':
			        require('language/lang.es.php');
			        $this->langArray = $lang;
			        break;       
			    default:
			        require('language/lang.en.php');
			        $this->langArray = $lang;
			        break;
		  }
		}
	 }
}

?>