<?php
	/**
	 *	LANGUAGE SUPPORT FOR ENGLISH
	 */

	$lang = array();
	//headers
	$lang['INDEX']['TITLE'] = 'SoundHub';
	//navbar
	$lang['INDEX']['NAV_ABOUT'] = 'About';
	$lang['INDEX']['NAV_EXPLORE'] = 'Explore';
	$lang['INDEX']['NAV_SEARCH'] = 'Search';
	$lang['INDEX']['NAV_REGISTER'] = 'Register';
	$lang['INDEX']['NAV_LOGIN'] = 'Login';
	//jumbotron text
	$lang['INDEX']['JUMBO_HEADER'] = 'Share your sounds';
	$lang['INDEX']['JUMBO_TEXT'] = 'The open way to compose and improve your art';
	$lang['INDEX']['JUMBO_LEARNMORE'] = 'Learn More';
	//
	$lang['INDEX']['FEATURES_TITLE'] = 'Just take a look';
  	//feature1
	$lang['INDEX']['FEATURE1_TITLE'] = 'Don\'t care about distances';
	$lang['INDEX']['FEATURE1_TEXT'] = 'We help you to find collaboration with people from all over the world!';
	//feature2
	$lang['INDEX']['FEATURE2_TITLE'] = 'Get the sound of others equipment';
	$lang['INDEX']['FEATURE2_TEXT'] = 'Obtain the best of the gear of your collaborators';
	//feature3
	$lang['INDEX']['FEATURE3_TITLE'] = 'Keep your budget';
	$lang['INDEX']['FEATURE3_TEXT'] = 'Get a free account and make until 5 private projects. Public projects are unlimited.<br> If you want to continue the private subscription, just pay a 1$ contribution per track';
	//Start now
	$lang['INDEX']['BTN_START_NOW'] = 'Start now';

?>