<?php  
	/**
	 *	SOPORTE DE LENGUAJE PARA ESPAÑOL
	 */

	$lang = array();
	//headers
	$lang['INDEX']['TITLE'] = 'SoundHub';

	//navbar
	$lang['INDEX']['NAV_ABOUT'] = 'Acerca de';
	$lang['INDEX']['NAV_EXPLORE'] = 'Explorar';
	$lang['INDEX']['NAV_SEARCH'] = 'Busqueda';
	$lang['INDEX']['NAV_REGISTER'] = 'Registrate';
	$lang['INDEX']['NAV_LOGIN'] = 'Iniciar sesión';

	//jumbotron text
	$lang['INDEX']['JUMBO_HEADER'] = 'Comparte tu sonido';
	$lang['INDEX']['JUMBO_TEXT'] = 'El medio libre para componer y mejorar tu música';
	$lang['INDEX']['JUMBO_LEARNMORE'] = 'Leer más';
	//
	$lang['INDEX']['FEATURES_TITLE'] = 'Solo echa un vistazo';
  	
  	//feature1
	$lang['INDEX']['FEATURE1_TITLE'] = 'No te preocupes por la distancia';
	$lang['INDEX']['FEATURE1_TEXT'] = 'Te ayudamos a encontrar colaboración con gente de todo el mundo';
	//feature2
	$lang['INDEX']['FEATURE2_TITLE'] = 'El sonido de otros';
	$lang['INDEX']['FEATURE2_TEXT'] = 'Obtén el sonido del arsenal de instrumentos de tus colegas';
	//feature3
	$lang['INDEX']['FEATURE3_TITLE'] = 'Ahorra presupuesto';
	$lang['INDEX']['FEATURE3_TEXT'] = 'Obtén una cuenta gratis y crea hasta 5 proyectos privados. Los proyectos publicos son ilimitados.<br> Para continuar creando en privado, solo pagas una contribución de 1$ por track';
	//Start now
	$lang['INDEX']['BTN_START_NOW'] = 'Empieza ahora';

?>