<?php
	require_once 'config.php';
	require_once ROOT_DIR.'\crud\project.db.php';
	require_once ROOT_DIR.'\crud\file.db.php';
	require ROOT_DIR.'\class\page.class.php';
	/**
	* 	CONTROLLER DE VISTA PARA LAS PÁGINAS RELATIVAS A UN PROYECTO
	* 	PARA EXTRACCIÓN DE DATOS USAR crud\project.db.php
	* 	
	*/
	class Project_control extends Project{
		
		var $role = 'default';
		var $conflicto;

		/**
		 * Inicialización de las variables requeridas
		 */
		function __construct(){
			$this->conflicto['user-default'] = (isset($_SESSION['user']['email'])) ? 
					'user' : 'default';	
		}

		/**
		 * Crea los elementos que llevaran las listas
		 * @return Array con los items
		 */
		function createItems($id=null){
			$projects = (is_null($id)) ? $this->read(false) : $this->readWhere($id);
			foreach ($projects as $key => $value) {
					$projects[$key]['public'] = 
						($projects[$key]['public'] == 1)
					 		? 'Público' : 'Privado';
			}
		  	//Cargamos el item de cada proyecto
		  	foreach ($projects as $project) {
		  		$item = new Template(TEMPLATES_PATH."/lists/projects_list_item.tpl.php");	
			    foreach ($project as $key => $value) {
			      $item->set($key,$value);
			    }
			    $projectTemplates[] = $item;
		  	}

		  	if (isset($projectTemplates)) {
		  		return Template::merge($projectTemplates);
		  	}else{
		  		return null;
		  	}


		}

		/**
		 * Imprime la lista de todos los projectos
		 * @return [type] [description]
		 */
		function list(){
			if (isset($_SESSION['user'])) {
			  	$project_contents = $this->createItems($_SESSION['user']['id_user']);
			  	if (is_null($project_contents)) {
		  			$page_final = $this->factory('list');
					$page_final->set('CONTENT',"No se han cargado proyectos aún");
			  	}else{
			  		$projects_list = new Template(TEMPLATES_PATH.'/lists/projects_list.tpl.php');
		  			$projects_list->set("projects",$project_contents);
		  			$page_final = $this->factory('list');
					$page_final->set('CONTENT',$projects_list->output());
			  	}
				$page_final->render();
				echo $page_final->output();	
			}else{
			  	$project_contents = $this->createItems();
				$projects_list = new Template(TEMPLATES_PATH.'/lists/projects_list.tpl.php');
		  		$projects_list->set("projects",$project_contents);
		  		$page_final = $this->factory('list');
				$page_final->set('CONTENT',$projects_list->output());
				$page_final->render();
				echo $page_final->output();	
			}
		}

		function audio_items(){
			$audios = new File();
			$audios->connectdb();
			$contents = $audios->list($_SESSION['current_project']['id_project']);
			foreach ($contents as $content) {
				$item = new Template(TEMPLATES_PATH."/lists/projects_audio_item.tpl.php");
				foreach ($content as $key => $value) {
					$item->set($key,$value);
				}
				$templates[] = $item;
			}
			if (isset($templates)) {
				return Template::merge($templates);
			}else{
				return null;
			}
			
		}
		/**
		 * Imprime la seccion audios de un proyecto
		 * @return [type] [description]
		 */
		function audios(){
			$contents = $this->audio_items();
			$page_final = $this->factory('audios');
			if ( isset($contents)) {
				$list = new Template(TEMPLATES_PATH.'/lists/projects_audio_list.tpl.php');
				$list->set("item",$contents);
				$page_final->set('CONTENT',$list->output());
			}else{
				$page_final->set('CONTENT','Aun no se han cargado archivos');	
			}
			$data = $this->readOne($_SESSION['current_project']['id_project']);
			$page_final->set('PROJECT',$data['title']);
			$page_final->set('info',$data['info']);
			$page_final->render();
			echo $page_final->output();
		}

		/**
		 * Imprime la seccion collaborators de un proyecto
		 * @return [type] [description]
		 */
		function collaborators(){
			$collaborators = $this->getCollaborators($_SESSION['current_project']['id_project']);
			$contents = Template::merge($this->getItems('projects_collaborators',$collaborators));
			$list = new Template(TEMPLATES_PATH.'/lists/projects_collaborators_list.tpl.php');
			$list->set('items',$contents);
			$page_final = $this->factory('collaborators');
			$page_final->set('CONTENT',$list->output());
			$page_final->render();
			echo $page_final->output();
		}
		
		/**
		 * Obtiene elementos de listados
		 * @param  [type] $classItem [tipo de item {projects | messages | favs | config }]
		 * @param  [type] $data      [array de datos]
		 * @return [type]            [description]
		 */
		function getItems($classItem,$data){
			$templates = array();
			foreach ($data as $node) {
				$item = new Template(TEMPLATES_PATH."/lists/".$classItem."_item.tpl.php");
				foreach ($node as $key => $value) {
			 		 $item->set($key,$value);
				}
				$templates[] = $item;
			}
			return $templates;
		}

		/**
		 * Imprime la seccion info de un proyecto
		 * @return [type] [description]
		 */
		function info(){
			$data = $this->readOne($_SESSION['current_project']['id_project']);
			$data['public'] = ($data['public'] == 1) ? 'Público' : 'Privado';
			$page_final = $this->factory('info');
			$page_final->set('id_project',$data['id_project']);
			$page_final->set('title',$data['title']);
			$page_final->set('public',$data['public']);
			$page_final->set('date_begin',$data['date_begin']);
			$page_final->set('info',$data['info']);
			$page_final->render();
			echo $page_final->output();
		}

		/**
		 * Imprime la seccion config de un proyecto
		 * @return [type] [description]
		 */
		function config(){
			$data = $this->readOne($_SESSION['current_project']['id_project']);
			$page_final = $this->factory('config');
			$page_final->set('id_project',$data['id_project']);
			$page_final->set('title',$data['title']);
			$page_final->set('info',$data['info']);
			if ($data['public'] == 1) {
				$page_final->set('checkPublic','checked');
			}else{
				$page_final->set('checkPrivate','checked');
			}
			$page_final->render();
			echo $page_final->output();
		}

		/**
		 * Renderiza de acuerdo a una accion determinada
		 * @param  String $action pagina principal a escribir en la ruta
		 * @return new Page 	  Objeto Pagina del que se obtiene un output
		 */
		function factory($action){
			$language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
			$page_final = new Page(TEMPLATES_PATH.'navbar-'.$this->conflicto['user-default'].'.tpl.php',
				                    TEMPLATES_PATH.$this->role.'project_'.$action.'.tpl.php',
				                    TEMPLATES_PATH.'footer.tpl.php',
				                    $language); 
			//$page_final->set("PROJECT", $_SESSION['current_project']['title']);
			$page_final->setFileContent('LEFT-NAVBAR',
				TEMPLATES_PATH.$this->role.'project_left_navbar.tpl.php');
			return $page_final;
		}

		/**
		 *	Borra registro de la base de datos y del sistema de ficheros
		 */
		function deleteFile($id_file){
			if ($this->projectRole($_SESSION['user']['id_user'],$_SESSION['current_project']['id_project']) === 'owner') {
				//Primero en el sistema de ficheros
				$file = new File();
				$file->connectdb();
				$register=$file->register($id_file);

				unlink($register['source']);
				//Borra de la base de datos
				$file->delete($register['id_file']);
				header("Location: project.php?action=audios");
			}else{
				//Validar si se tiene permiso para acceder al proyecto, 
				// id_user, id_project, id_role 
				$id_project = $_SESSION['current_project']['id_project'];
				$this->route($id_project);
				$this->audios();
				echo '<script>alert("No puedes borrar")</script>';

			}
		}

		/**
		 * Lleva al usuario actual a la página correcta del 
		 * proyecto que solicitó
		 * @param  [type] $id_project [description]
		 * @return [type]             [description]
		 */
		function route($id_project = null){
			if (is_null($id_project)) {
				$this->role='project/';
			}else{
				$this->role = (isset($_SESSION['user']['id_user'])) ? 
					'project/'.$this->projectRole($_SESSION['user']['id_user'],$id_project).'/'
					: 'project/default/';
			}
		}

		/**
		 * Renderiza el formulario de añadir colaborador
		 * @return [type] [description]
		 */
		function collaborator_form(){
			$page_final = $this->factory('addCollaborator');
			$page_final->render();
			echo $page_final->output();
		}

		function updateFile($id_file){
			$db = new File();
			$db->connectdb();
			$data = $db->readOne($id_file);
			$page_final = $this->factory('updateFile');
			foreach ($data as $key => $value) {
				$page_final->set($key,$value);
			}
			$page_final->render();
			echo $page_final->output();
		}
	}

	
	$web = new Project_control();
	$web->connectdb();

	if (isset($_SESSION)) {
		$_SESSION['current_project']['id_project']= ( isset($_GET['id_project']) ) ? 
			$_GET['id_project'] : (isset($_SESSION['current_project']['id_project']) ? 
									$_SESSION['current_project']['id_project'] : 
									0
			);
	}

	switch ($_REQUEST['action']) {
		case 'new_collaborator':
			$web->addCollaborator($_SESSION['current_project']['id_project'],$_POST['username']);
			$id_project = $_SESSION['current_project']['id_project'];
			//FALTA RENDERIZAR, la consulta ya se hace correctamente
			$web->route($id_project);
			$web->collaborators();
			break;
		case 'addCollaborator':
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->collaborator_form();			
			break;
		case 'updateData':
			$id_project = $_SESSION['current_project']['id_project'];
			$web->update($id_project,$_POST);
			echo "<script>alert('Los datos se guardaron exitosamente')</script>";
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->info();
		break;
		case 'updateFile':
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->updateFile($_POST['id_file']);
			break;
		case 'updateDataFile':
			$file = new File;
			$file->connectdb();
			$file->update($_POST);
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->audios();	
			break;
		case'delete':
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->deleteFile($_REQUEST['id_file']);
			break;
		case'read':
			$web->route();
			$web->list();
			break;
		case 'audios':
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->audios();	
			break;
		case 'collaborators':
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->collaborators();
			break;
		case 'info':
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->info();
			break;
		case 'config':
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->config();
			break;
		case 'new_file':
			header("Location: file.php?action=new");
			break;
		//Leer solo un project con la página de audios como default
		default:
			//Validar si se tiene permiso para acceder al proyecto, 
			// id_user, id_project, id_role 
			$id_project = $_SESSION['current_project']['id_project'];
			$web->route($id_project);
			$web->audios();
			break;
	}
?>