<?php
  require_once 'config.php';
  require_once 'class/login.class.php';
  require_once 'crud/user.db.php';
  require_once 'view.php';
  require_once 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
  require_once 'class/page.class.php';

  class Login_control extends Login{
    /**
     * login user
     * @param  [type] $email    [description]
     * @param  [type] $password [description]
     * @return [type]           [description]
     */
    function login($email,$password){
      $credentials = array($_POST['email'],$_POST['password']);
      list($email,$password) = $credentials;
      if(parent::validate($email,$password)){
        $bd = new User;
        $bd->connectdb();
        if ($bd->isRole($email,'common')) {
          $_SESSION['logged'] = true;
          $_SESSION['user'] = $bd->userData($email);
          $_SESSION['roles'] = $bd->roles($email);
          $_SESSION['user_project']=$bd->user_project($_SESSION['user']['id_user']);
          header("location: user.php?action=projects");
        }
      }else{
        echo "<script>alert('La contraseña no es valida');</script>";
        View::render('login');
      }
    }

    /**
     * [register description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    function register($data){
      $userdata = array();
      $userdata['username'] = $data['username'];
      $userdata['name'] = $data['name'];
      $userdata['lastname'] = $data['lastname'];
      $userdata['email'] = $data['email'];
      $pass = $data['password'];
      $passcheck = $data['password_check'];

      if (strcmp($pass, $passcheck) !== 0) {
        echo "<script>alert('Las contraseñas no son iguales')</script>";
        die();
      }else{
        $hash_password  = password_hash($_POST['password'],PASSWORD_DEFAULT);
        $userdata['password'] = $hash_password;
      }
      $userdata['activation_hash'] = md5( rand(0,1000) );
      $userdata['active'] = 0;

      if ($this->email($userdata)) {
        //Guardar en la base de datos
        $user = new User();
        $user->connectdb();
        $user->create($userdata);

        $sql = "INSERT INTO user_role (id_user,id_role) 
          VALUES (
            (SELECT id_user FROM user WHERE username = :username),
            (SELECT id_role FROM role WHERE role = 'common')
          )";
        $statement = $user->bd->prepare($sql);
        $statement->bindParam(":username",$userdata['username']);
        $statement->Execute();
        header("Location: confirmation.php?action=creation");
      }else{
        echo "<script>alert('Error, no se ha podido crear la cuenta, error en el servidor')</script>";
      }
    }

    /**
     * Escribe un email de confirmación
     * @param  [type] $userdata [description]
     * @return [type]           [description]
     */
    function email($userdata){
        date_default_timezone_set('Etc/UTC');

        $mail = new PHPMailer;
        $mail->isSMTP();
        //$mail->SMTPDebug = 2;
        //$mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';        
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = "14030620@itcelaya.edu.mx";
        $mail->Password = "zab112358";
        $mail->setFrom('noreply@soundhub.com', 'Webmaster');
        $mail->addAddress($userdata['email'], $userdata['username']);
        $mail->Subject = 'Register on soundhub';
        $mail->msgHTML(
          'Sigue el enlace para activar tu cuenta y poder logearte 
          <a href=localhost/soundhub/user.php?action=verify&email='.$userdata['email'].'&username='.$userdata['username'].'&hash='.$userdata['activation_hash'].'>Link</a>');
        $mail->AltBody =           
          'Sigue el enlace para activar tu cuenta y poder logearte 
          localhost/soundhub/user.php?action=verify&email='.$userdata['email'].'&username='.$userdata['username'].'&hash='.$userdata['activation_hash'];

        return $mail->send();
    }

    function reset($email){
      $string = md5($email.'recuperacion'.rand(1,10000));

      $sql = "UPDATE user set recovery=:recovery WHERE email=:email";
      $statement = $this->bd->prepare($sql);
      $statement->bindParam(':recovery',$string);
      $statement->bindParam(':email',$email);
      $statement->Execute();

      $mail = new PHPMailer;
      $mail->isSMTP();
      // $mail->SMTPDebug = 2;
      // $mail->Debugoutput = 'html';
      $mail->Host = 'smtp.gmail.com'; 
      $mail->Port = 587;
      $mail->SMTPSecure = 'tls';
      $mail->SMTPAuth = true;
      $mail->Username = "14030620@itcelaya.edu.mx";
      $mail->Password = "zab112358";
      $mail->setFrom('14030620@itcelaya.edu.mx', 'WebMaster');
      $mail->addAddress($email, 'Usuario mediweb');
      $mail->Subject = 'Recuperacion de contrasena';
      $mensaje_reply = "Haga <a href=http://localhost/soundhub/login.php?action=new_password&key=$string>Clic aqui</a>";
      $mail->msgHTML($mensaje_reply);
      $mail->AltBody = 'href=http://localhost/soundhub/login.php?action=new_password&key=$string';
      if ($mail->send()) {
          echo "<script>Se ha enviado un mensaje de recuperacion, revisa tu correo</script>";
          View::render("login");
      } 

    }

    /**
     * [finalize description]
     * @return [type] [description]
     */
    function finalize(){
      $this->logout();
      header('Location: index.php');
    }

    function newPassword($key){
      $sql  = "SELECT * FROM user WHERE recovery=:recovery";
      $statement = $this->bd->prepare($sql);
      $statement->bindParam(":recovery",$key);
      $statement->Execute();

      $datos=$statement->fetchAll(PDO::FETCH_ASSOC);
      if(sizeof($datos)>0){
        $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $page_final = new Page(TEMPLATES_PATH.'navbar-default.tpl.php',
                              TEMPLATES_PATH.'reset_password_form.tpl.php',
                              TEMPLATES_PATH.'footer.tpl.php',
                              $language); 
        //$page_final->set("PROJECT", $_SESSION['current_project']['title']);
        $page_final->set('id',$key);
        $page_final->render();
        echo $page_final->output();
      }
    }

    function setPassword($recovery,$password){
      $sql  = "SELECT * FROM user WHERE recovery=:recovery";
      $statement = $this->bd->prepare($sql);
      $statement->bindParam(":recovery",$recovery);
      $statement->Execute();
      $datos=$statement->fetchAll(PDO::FETCH_ASSOC);
      if(sizeof($datos)>0){
        $password = password_hash($password,PASSWORD_DEFAULT);
        $sql = "UPDATE user SET password=:password, recovery=null
            WHERE recovery=:recovery";
        $statement = $this->bd->prepare($sql);
        $statement->bindParam(':password',$password);
        $statement->bindParam(':recovery',$recovery);
        $statement->Execute();
        View::render('login');
      }
    }
}

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
if (isset($_REQUEST['email'])) {
  $email = isset($_POST['submit']) ? $_POST['email'] : null;
}
if (isset($_REQUEST['password'])) {
  $password = isset($_POST['submit']) ? $_POST['password'] : null;
}


$web = new Login_control;

switch ($action) {
  case 'register':
    View::render('register');
    break;
  case 'login':
    View::render('login');
    break;
    case 'validate':
      $web->login($email,$password);
    break;
  case 'create':
    $web->register($_POST);
    break;
  case 'logout':
    $web->finalize();
    break;
  case 'recovery':
    View::render("recovery");
    break;
  case 'reset':
    $web->reset($_REQUEST['email']);
    break;
  case "new_password":
    $web->newPassword($_GET['key']);
    break;
  case 'set_password':
    $key = $_REQUEST['key'];
    $contrasena = $_REQUEST['password'];
    $web->setPassword($key,$contrasena);
    break;
  default:
    break;
}
  
?>