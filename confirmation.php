<?php

  /**
   *  VIEW-CONTROLLER Mensaje de confirmación de registro de cuenta,se envia un hash para
   *  activarla
   */
  
  require 'config.php';
  require CLASSPATH.'page.class.php';
  require_once 'view.php';

  switch ($_GET['action']) {
    case 'creation':
        View::render("user_new");
      break;
    case 'activation':
        View::render("confirmation_message");
      break;
    default:
      # code...
      break;
  }
?>