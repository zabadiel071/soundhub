<?php 
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';

	class Index extends Database{
		
	}

	if (isset($_SESSION['crud'])) {
		$web = new Index;
		$web->connectdb();
		$web->access('admin');
		include 'templatescrud/index.php';
	}else{
		header("Location: login.php");
	}
	
?>