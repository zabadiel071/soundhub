<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';

	class Permission extends Database{
		
		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [description]
		 * @return [type]          [description]
		 */
		function create($data,$header = false){
			$sql = "INSERT INTO permission(permission) VALUES (:permission)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":permission",$data['permission']);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Obtiene un array con los datos de todos los users  
		 * @param header, true->carga el read(true);
		 * @return SELECT FROM users
		**/
		function read($header=false){
			$data = array();
			$sql = 'SELECT id_permission,permission from permission';
			$data = $this -> query($sql);
			if ($header)
				$this->render("permission_read.php",$data);
			else
				return $data;
		}


		function readOne($id,$header=false){
			$sql = 'SELECT * from permission WHERE id_permission=:id_permission';
			$statement = $this -> bd -> prepare($sql);
			$statement->bindParam(':id_permission',$id);
			$statement -> Execute();
			if (!$header) 
				return $statement->FetchAll(PDO::FETCH_ASSOC);;
		}

		/**
		 * Actualiza el registro con el id_permission
		 * @return filas actualizadas
		**/
		function update($id_permission,$data,$header = false){
			$sql="UPDATE permission 
				SET permission=:permission
					WHERE id_permission=:id_permission";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':permission',$data['permission']);
				$statement->bindParam(':id_permission',$id_permission);
			$statement->Execute(); 
			if($header)
				$this->read(true);
			else
				return $statement;
			
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_permission,$header = false){
			$sql = "DELETE FROM permission WHERE id_permission=:id_permission";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_permission",$id_permission);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
			
		}
		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('permission_form.php', $data, $action, $id);
		} 
 	
	}

		if (isset($_SESSION['crud'])){
			$web = new Permission;
			$web->connectdb();
			$web->access('admin');
	
			$action = (isset($_GET['action'])) ? $_GET['action'] : '';

			switch ($action) {
			case 'edit':
				$params['id_permission']=$_GET['id_permission'];
				$permission=$web->fetchAll("SELECT * FROM permission",$params);
				$data=$permission[0];
				$web->form('update',$_GET['id_permission'],$data);
				break;		
			case 'update':
				$web->update($_GET['id_permission'],$_GET['data'],true);
				break;
			case 'create':
				$data = $_GET['data'];
				$web->create($data,true);
				break;
			case 'new':
				$web->form();
				break;
			case 'readOne':
				$web->readOne($_GET['id_permission']);			
				break;
			case 'delete':
				$web->delete($_GET['id_permission'],true);
				break;
			default:
				$web->read(true);
				break;
		}
	}
?>