<?php 
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';

 	class Role extends Database{
		
		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [description]
		 * @return [type]          [description]
		 */
		function create($data,$crud = false,$header = false){
			$sql = "INSERT INTO role(role) VALUES (:role)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":role",$data['role']);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Obtiene un array con los datos de todos los users  
		 * @param header, true->carga el read(true);
		 * @return SELECT FROM users
		**/
		function read($header=false){
			$data = array();
			$sql = 'SELECT id_role,role from role';
			$data = $this -> query($sql);
			if ($header)
				$this->render("role_read.php",$data);
			else
				return $data;
		}

		/**
		 * Obtiene un array con los datos de un user
		 * @param id_user : datos del user del cual se buscan los datos
		**/
		function readOne($id_role,$header=false){
			$role = array();
			$sql = 'SELECT id_role,role,id_role_type from role WHERE id_role=:id_role';
			$statement = $this -> bd -> prepare($sql);
			$statement->bindParam(':id_role',$id_role);
			$statement -> Execute();
			$role=$statement->FetchAll(PDO::FETCH_ASSOC);
			$permissions = $this->permissions($id_role);
			$role['permission'] =$permissions;
			if ($header) 
				$this->render('role_readOne.php',$role);
			else
				return $role;
		}

		/**
		 * Permissions of role
		 * @param  [type] $id_role [description]
		 * @return [type]          [description]
		 */
		function permissions($id_role){
			$sql = "SELECT id_permission,permission FROM permission JOIN role_permission USING (id_permission) WHERE id_role=:id_role";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_role",$id_role);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Actualiza el registro con el id_role
		 * @return filas actualizadas
		**/
		function update($id_role,$data,$header=false){
			$sql="UPDATE role 
				SET role=:role
					WHERE id_role=:id_role";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':role',$data['role']);
				$statement->bindParam(':id_role',$id_role);
			$statement->Execute(); 
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_role,$header=false){
			$sql = "DELETE FROM role WHERE id_role=:id_role";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_role",$id_role);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}
		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('role_form.php', $data, $action, $id);
		} 
 	
	}


	$action=null;
	if (isset($_GET['action'])) {
		$action=$_GET['action'];
	}
	if (isset($_SESSION['crud'])){
		$web = new Role;
		$web->access('admin');
		$web->connectdb();
		switch ($action) {
			case 'edit':
					$params['id_role']=$_GET['id_role'];
					$role=$web->fetchAll("SELECT * FROM role",$params);
					$data=$role[0];
					$web->form('update',$_GET['id_role'],$data);
				break;		
			case 'update':
					$web->update($_GET['id_role'],$_GET['data'],true);
				break;
			case 'create':
				$data = $_GET['data'];
				$web->create($data,true,true);
				break;
			case 'new':
				$web->form();
				break;
			case 'readOne':
				$web->readOne($_GET['id_role'],true);			
				break;
			case 'delete':
				$web->delete($_GET['id_role'],true);
				break;
			default:
				$web->read(true);
				break;
		}
	}
?>