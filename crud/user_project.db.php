<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';
	/**
	* 
	*/
	class User_project extends Database{
		/**
		 * Crea un registro de un project en la tabla project
		 * @param data: array con los datos del nuevo project
		**/
		function create($data){
			$sql = "INSERT INTO user_project (id_project,id_user,id_role)
					VALUES (:id_project,:id_user,:id_role)";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':id_project',$data['id_project']);
				$statement->bindParam(':id_user',$data['id_user']);
				$statement->bindParam(':id_role',$data['id_role']);
			$statement -> Execute();
		}

		function namesCreate($title,$username,$role){
			$sql = "INSERT INTO user_project (id_project,id_user,id_role)
					VALUES (
						(SELECT id_project FROM project WHERE title=:title),
						(SELECT id_user FROM user WHERE username=:username),
						(SELECT id_role FROM role WHERE role=:role)
					)";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':title',$title);
				$statement->bindParam(':username',$username);
				$statement->bindParam(':role',$role);
			$statement -> Execute();
			return $statement;
		}

		function collab_details($id_project){
			$sql = "SELECT username,email,name as collaborator_name,lastname,r.role FROM user u 
						JOIN user_project up USING (id_user)
						JOIN role r USING (id_role)
						WHERE id_project=:id_project";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(':id_project',$id_project);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);
		}

		function read($header = false){
			$sql = 'SELECT id_user_project,id_project,title,id_user,username,id_role,role from user_project JOIN user 
					USING(id_user) JOIN role USING(id_role) JOIN project USING(id_project)';
			$user_project = $this -> query($sql);
			if ($header)
				$this->render("user_project_read.php",$user_project);
			else
				return $user_project;
		}

		function readOne($id_user_project){
			$sql = 'SELECT id_user_project,id_project,title,id_user,username,id_role,role from user_project JOIN user 
					USING(id_user) JOIN role USING(id_role) JOIN project USING(id_project) WHERE id_user_project=:id_user_project';
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(':id_user_project',$id_user_project);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);
		}

		function readSome($title){
			$sql = 'SELECT id_user_project,id_project,title,id_user,username,id_role,role from user_project JOIN user 
					USING(id_user) JOIN role USING(id_role) JOIN project USING(id_project) WHERE title=:title';
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(':title',$title);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);

		}

		function update($id_user_project,$data){
			$sql = "UPDATE user_project SET
					id_user=(SELECT id_user FROM user WHERE username=:username),
					id_project=(SELECT id_project FROM project WHERE title=:title),
					id_role=(SELECT id_role FROM role WHERE role=:role)
					WHERE id_user_project = :id_user_project
					";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(':username',$data['username']);
			$statement->bindParam(':title',$data['title']);
			$statement->bindParam(':role',$data['role']);
			$statement->bindParam(':id_user_project',$id_user_project);
			$statement->Execute();
			return $statement;
		}

		function delete($data){
			$sql = 'DELETE FROM user_project WHERE id_project=:id_project 
											AND id_user=:id_user
											AND id_role=:id_role';
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(':id_project',$data['id_project']);
			$statement->bindParam(':id_user',$data['id_user']);
			$statement->bindParam(':id_role',$data['id_role']);
			$statement->Execute();
		}

		function deleteID($id){
			$sql = 'DELETE FROM user_project WHERE id_user_project=:id';
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(':id',$id);
			$statement->Execute();
			return $statement;
		}
	}


	if (isset($_GET['action'])) {
		$action=$_GET['action'];
	}
	if (isset($_SESSION['crud'])){
		$web = new User_project;
		$web->access('admin');
		$web->connectdb();
		$action=null;
		switch ($action) {
			case 'create':
				$data = array('id_user' => $_GET['id_user'],
							  'id_role' => $_GET['data']['id_role']);
				$web->create($data,true,true);
				break;
			case 'new':
				$id['id_user'] = $_GET['id_user'];
				$web->form('create',$id);
				break;
			case 'delete':
				$web->delete($_GET);
				$web->read(true);
				break;
			default:
				$web->read(	true);
				break;
		}
	}
		
?>