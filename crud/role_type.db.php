<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';

 	class Role_Type extends Database{
		
		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [True->reenvia al read(), false->retorna la sentencia preparada (usada en los servicios)]
		 * @return [type]          [description]
		 */
		function create($data,$header = false){
			$sql = "INSERT INTO role_type(role_type) VALUES (:role_type)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":role_type",$data['role_type']);
			$statement->Execute();
			if($header)
				$this->read();	
			else
				return $statement;				
		}

		/**
		 * Obtiene un array con los datos de todos los users  
		 * @param show, true->carga el read();
		 * @return SELECT FROM users
		**/
		function read($show=true){
			$data = array();
			$sql = 'SELECT id_role_type,role_type from role_type';
			$data = $this -> query($sql);
			if ($show)
				$this->render("role_type_read.php",$data);
			else
				return $data;
		}


		/**
		 * Actualiza el registro con el id_role
		 * @return filas actualizadas
		**/
		function update($id_role_type,$data,$header = false){
			$sql="UPDATE role_type 
				SET role_type=:role_type
					WHERE id_role_type=:id_role_type";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':role_type',$data['role_type']);
				$statement->bindParam(':id_role_type',$id_role_type);
			$statement->Execute(); 
			if($header)
				$this->read();
			else
				return $statement;
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_role_type,$header = false){
			$sql = "DELETE FROM role_type WHERE id_role_type=:id_role_type";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_role_type",$id_role_type);
			$statement->Execute();
			if($header)
				$this->read();
			else
				return $statement;
		}
		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('role_type_form.php', $data, $action, $id);
		} 
 	
	}


	$action=null;
	if (isset($_GET['action'])) {
		$action=$_GET['action'];
	}
	if (isset($_SESSION['crud'])){
		$web = new Role_Type;
		$web->access('admin');
		$web->connectdb();
		switch ($action) {
			case 'edit':
					$params['id_role_type']=$_GET['id_role_type'];
					$role_type=$web->fetchAll("SELECT * FROM role_type",$params);
					$data=$role_type[0];
					$web->form('update',$_GET['id_role_type'],$data);
				break;		
			case 'update':
					$web->update($_GET['id_role_type'],$_GET['data'],true);
				break;
			case 'create':
				$data = $_GET['data'];
				$web->create($data,true);
				break;
			case 'new':
				$web->form();
				break;
			case 'delete':
				$web->delete($_GET['id_role_type'],true);
				break;
			default:
				$web->read();
				break;
		}
	}
	
?>