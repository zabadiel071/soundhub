<?php 
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';
	/**
	* 
	*/
	class User extends Database{
		
		function getCollaborations($id_user){
			$sql = "SELECT * from project p JOIN user_project up USING (id_project)
											JOIN role r using(id_role)
											WHERE up.id_user=:id_user AND role='collaborator'";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_user",$id_user);
			$statement->Execute();					
			$array = $statement->FetchAll(PDO::FETCH_ASSOC);
			return $array;
		}

		function getID($email,$username=null){
			if (!is_null($email)) {
				$sql = "SELECT id_user FROM user WHERE email='".$email."'";
				$array = $this->query($sql);
				return $array[0]['id_user'];
			}
			if (!is_null($username)) {
				$sql = "SELECT id_user FROM user WHERE username=:username";
				$statement = $this->bd->prepare($sql);
				$statement->bindParam(":username",$username);
				$statement->Execute();
				$array = $statement->FetchAll(PDO::FETCH_ASSOC);
				return $array[0]['id_user'];
			}
		}


		/**
		 * Obtiene datos para guardarlos en array de session
		 * @param  [type] $email [description]
		 * @return [type]        [description]
		 */
		function userData($email){
			$sql = "SELECT id_user,username,name,lastname,email FROM user WHERE email='".$email."'";
			$array = $this->query($sql);
			return $array[0];
		}


		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [description]
		 * @return [type]          [description]
		 */
		function create($data,$crud = false,$header = false){
			$sql = "INSERT INTO user(username,name,lastname,email,password,activation_hash,active) values (:username,:name,:lastname,:email,:password,:activation_hash,:active)";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':username',$data['username']);
				$statement->bindParam(':email',$data['email']);
				$statement->bindParam(':name',$data['name']);
				$statement->bindParam(':lastname',$data['lastname']);
				if ($crud) {
					$data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
					$data['activation_hash'] = md5( rand(0,1000) );
					$data['active'] = 1;
				}
				$statement->bindParam(':password',$data['password']);
				$statement->bindParam(':activation_hash',$data['activation_hash']);
				$statement->bindParam(':active',$data['active']);
				$statement -> Execute();
				if ($header)
					$this->read(true);
				else
					return $statement;
		}

		/**
		 * Obtiene un array con los datos de todos los users  
		 * @param show, true->carga el read(true);
		 * @return SELECT FROM users
		**/
		function read($header=false){
			$sql = 'SELECT id_user,username,name,lastname,email,active from user';
			$data = $this -> query($sql);
			if ($header)
				$this->render("user_read.php",$data);
			else
				return $data;
		}


		/**
		 * Obtiene un array con los datos de un user
		 * @param id_user : datos del user del cual se buscan los datos
		**/
		function readOne($id_user,$header=false){
			$user = array();
			$sql = 'SELECT id_user,username,name,lastname,email,active from user WHERE id_user=:id_user';
			$statement = $this -> bd -> prepare($sql);
			$statement->bindParam(':id_user',$id_user);
			$statement -> Execute();
			$user=$statement->FetchAll(PDO::FETCH_ASSOC);
			$roles = $this->roles($user[0]['email']);
			$user['rol'] = $roles;
			if ($header) 
				$this->render('user_readOne.php',$user);
			else
				return $user;
		}

		/**
		 * Actualiza el registro con el id_user
		 * @return filas actualizadas
		**/
		function update($id_user,$data,$header = false){
			$sql="UPDATE user 
				SET username=:username,
					email=:email,
					name=:name,
					lastname=:lastname,
					email=:email
					WHERE id_user=:id_user";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':username',$data['username']);
				$statement->bindParam(':email',$data['email']);
				$statement->bindParam(':name',$data['name']);
				$statement->bindParam(':lastname',$data['lastname']);
				$statement->bindParam(':email',$data['email']);
				$statement->bindParam(':id_user',$id_user);
			$statement->Execute(); 
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		function activation($data,$code){
			$sql = "UPDATE user SET active=:active
					WHERE email=:email AND username=:username AND activation_hash=:activation_hash";
			$statement=$this -> bd -> prepare($sql);
				$statement->bindParam(':active',$code,PDO::PARAM_INT);
				$statement->bindParam(':email',$data['email']);
				$statement->bindParam(':username',$data['username']);
				$statement->bindParam(':activation_hash',$data['hash']); 
				$statement -> Execute();
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_user,$header=false){
			$sql = "DELETE FROM user WHERE id_user=:id_user";
			$statement = $this -> bd -> prepare($sql);
			$statement -> bindParam(':id_user',$id_user);
			$statement -> Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}
		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('user_form.php', $data, $action, $id);
		} 

		function roles($email){
			$sql = "SELECT id_role,role FROM role join user_role using(id_role) 
						WHERE id_user=
								(SELECT id_user FROM user WHERE email=:email)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(':email',$email);
			$statement->Execute();
			$roles = $statement->FetchAll(PDO::FETCH_ASSOC);
			return $roles;
		}

		/**
		 * Retorna si un user es administrador (Tiene derecho a entrar al CRUD)
		 * @param  [type] $email    [description]
		 * @param  [type] $password [description]
		 * @return [type]           [description]
		 */
		function isRole($email , $search_role){
			$bd = new User;
			$bd->connectdb();
			$roles = $this->roles($email);
			$flag = false;
			foreach ($roles as $value) {
				if ($value['role'] == $search_role ) {
					$flag = true; 
					break;
				}
			}
			return $flag;
		}

		function user_project($id_user){
			$sql = "SELECT * FROM user_project WHERE id_user=:id_user";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_user",$id_user);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);
		}
		
	}

	if (isset($_SESSION['crud'])){
		$web = new User;
		$web->connectdb();
		$web->access('admin');

		$action = (isset($_GET['action'])) ? $_GET['action'] : '';
		switch ($action) {
			case 'edit':
				$params['id_user']=$_GET['id_user'];
				$user=$web->fetchAll("SELECT * FROM user",$params);
				$data=$user[0];
				$web->form('update',$_GET['id_user'],$data);
			break;		
			case 'update':
					$web->update($_GET['id_user'],$_GET['data'],true);
				break;
			case 'create':
					$data = $_GET['data'];
					$web->create($data,true,true);
				break;
			case 'new':
					$web->form();
				break;
			case 'delete':
				if (isset($_GET['id_user'])) {
					$id_user = $_GET['id_user'];
					$web->delete($id_user,true);
				}
			break;				
			case 'readOne':
				if(isset($_GET['id_user']))
					$web->readOne($_GET['id_user'],true);
				break;
			default:
				$web->read(true);
				break;
		}
	}
?>