<?php  
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';

 	class Instrument extends Database{
		
		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [description]
		 * @return [type]          [description]
		 */
		function create($data,$header = false){
			$sql = "INSERT INTO instrument(instrument) VALUES (:instrument)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":instrument",$data['instrument']);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Obtiene un array con los datos de todos los users  
		 * @param header, true->carga el read(true);
		 * @return SELECT FROM users
		**/
		function read($header=false){
			$instrument = array();
			$sql = 'SELECT id_instrument,instrument from instrument';
			$instrument = $this -> query($sql);
			if ($header)
				$this->render("instrument_read.php",$instrument);
			else
				return $instrument;
		}

		/**
		 * Obtiene un Instrument
		 * @param  [type] $id [description]
		 * @return [type]     [description]
		 */
		function readOne($id,$header = false){
			$sql = "SELECT * FROM instrument WHERE id_instrument=:id";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id",$id);
			$statement->Execute();
			if(!$header)
				return $statement->FetchAll(PDO::FETCH_ASSOC);
		}


		/**
		 * Actualiza el registro con el id_role
		 * @return filas actualizadas
		**/
		function update($id_instrument,$data,$header = false){
			$sql="UPDATE instrument 
				SET instrument=:instrument
					WHERE id_instrument=:id_instrument";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':instrument',$data['instrument']);
				$statement->bindParam(':id_instrument',$id_instrument);
			$statement->Execute(); 
			if ($header) 
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_instrument,$header=false){
			$sql = "DELETE FROM instrument WHERE id_instrument=:id_instrument";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_instrument",$id_instrument);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}
		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('instrument_form.php', $data, $action, $id);
		} 
 	
	}

	$action=null;
	if (isset($_GET['action'])) {
		$action=$_GET['action'];
	}
	if (isset($_SESSION['crud'])){
		$web = new Instrument;
		$web->access('admin');
		$web->connectdb();
		switch ($action) {
			case 'edit':
					$params['id_instrument']=$_GET['id_instrument'];
					$instrument=$web->fetchAll("SELECT * FROM instrument",$params);
					$data=$instrument[0];
					$web->form('update',$_GET['id_instrument'],$data);
				break;		
			case 'update':
					$web->update($_GET['id_instrument'],$_GET['data'],true);
				break;
			case 'create':
				$data = $_GET['data'];
				$web->create($data,true);
				break;
			case 'new':
				$web->form();
				break;
			case 'delete':
				$web->delete($_GET['id_instrument'],true);
				break;
			default:
				$web->read(true);
				break;
		}
	}
?>
