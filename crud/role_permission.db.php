<?php 
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';

 	class Role_Permission extends Database{
		
		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [description]
		 * @return [type]          [description]
		 */
		function create($data,$crud = false,$header = false){
			$sql = "INSERT INTO role_permission(id_role,id_permission) VALUES (:id_role,:id_permission)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_role",$data['id_role']);
			$statement->bindParam(":id_permission",$data['id_permission']);
			$statement->Execute();
			header("Location: role.db.php?action=readOne&id_role=".$data['id_role']);
		}

		function namesCreate($role,$permission){
			$sql = "INSERT INTO role_permission(id_role,id_permission) 
				VALUES((SELECT id_role FROM role WHERE role=:role limit 1),
					   (SELECT id_permission FROM permission WHERE permission=:permission limit 1))";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":role",$role);
			$statement->bindParam(":permission",$permission);
			$statement->Execute();
			return $statement;
		}

		function read($header=false){
			$sql = "SELECT id_role_permission,id_role,role,id_permission,permission FROM role_permission JOIN role using (id_role) JOIN permission using(id_permission)";
			$data = $this -> query($sql);
			return $data;
		}

		function readOne($id){
			$sql = "SELECT id_role_permission,id_role,role,id_permission,permission FROM role_permission JOIN role using (id_role) JOIN permission using(id_permission) WHERE id_role_permission=:id_role_permission";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_role_permission",$id);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);
		}	

		function readSome($id_role,$id_permission){
			$sql = "SELECT * FROM role_permission WHERE id_role=:id_role AND id_permission=:id_permission";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_role",$id_role);
			$statement->bindParam(":id_permission",$id_permission);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);
		}

		function update($id,$data){
			$sql = "UPDATE role_permission 
					SET id_role=(SELECT id_role FROM role WHERE role=:role),
						id_permission=(SELECT id_permission FROM permission WHERE permission=:permission)
					WHERE id_role_permission=:id_role_permission";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":role",$data['role']);
			$statement->bindParam(":permission",$data['permission']);
			$statement->bindParam(":id_role_permission",$id);
			$statement->Execute();
			return $statement;
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_role,$id_permission,$header=false){
			$sql = "DELETE FROM role_permission WHERE id_role=:id_role AND id_permission=:id_permission";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_role",$id_role);
			$statement->bindParam(":id_permission",$id_permission);
			$statement->Execute();
			if($header)
				header("Location: role.db.php?action=readOne&id_role=".$data['id_role']);	
			else
				return $statement;
		}

		function namesDelete($role,$permission){
			$sql = "DELETE FROM role_permission WHERE
					id_role=(SELECT id_role FROM role WHERE role=:role) AND
					id_permission=(SELECT id_permission FROM permission WHERE permission=:permission)
					";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":role",$role);
			$statement->bindParam(":permission",$permission);
			$statement->Execute();
			return $statement;
		}

		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('role_permission_form.php', $data, $action, $id);
		} 
 	
	}


	if (isset($_SESSION['crud'])){
		$web = new Role_Permission;
		$web->connectdb();
		$web->access('admin');

		$action = (isset($_GET['action'])) ? $_GET['action'] : '';
		switch ($action) {
				case 'create':
					$data = array('id_role' => $_GET['id_role'],
								  'id_permission' => $_GET['data']['id_permission']);
					$web->create($data,true,true);
					break;
				case 'new':
					$id['id_role'] = $_GET['id_role'];
					$web->form('create',$id);
					break;
				case 'delete':
					$web->delete($_GET['id_role'],$_GET['id_permission'],true);
					break;
			}
	}
	
?>