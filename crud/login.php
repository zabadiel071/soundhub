<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'login.class.php';
	require_once 'user.db.php';

	class Login_crud extends Login{

		function login($email,$password){
			if ($this->validate($email,$password)) {
				//Recuperar los datos del usuario
				$bd = new User;
				$bd->connectdb();
				if ($bd->isRole($email,'admin')) {
					$datos = $bd->userData($email);
					$_SESSION['logged']=true;
					$_SESSION['crud']=true;
					$_SESSION['email']=$datos['email'];
					$_SESSION['id_user']=$datos['id_user'];
					$roles = $bd->roles($email);
					$_SESSION['roles']=$roles;
					//Obtener los privilegios
					//$_SESSION['privilegios']=$privilegios;
					header("Location: index.php");
				}else{
					echo "<script>alert('No es administrador del sistema');</script>";
					$this->form();
				}
			}else{
				echo "<script>alert('La contraseña no es valida');</script>";
				$this->form();
			}
		}

		function form($action = "login"){
			$this->render('login_form.php',null,$action,null); 
		}
	}

	$web = new Login_crud;
	$web->connectdb();
	$action=null;

	if (isset($_REQUEST['action'])) {
		$action=$_REQUEST['action'];
	}
	switch ($action) {
		case 'set_password':
			break;
		case 'new_password':
			break;
		case 'login':
			if(isset($_REQUEST['email']) & isset($_REQUEST['password']))
				$web->login($_REQUEST['email'],$_REQUEST['password']);
			break;
		case 'logout':
				$web->logout();
				header("Location: index.php");
			break;
		case 'resetPassword':
			break;
		case 'delete':
			break;
		case 'recovery':
			break;
		default:
			$web->form();
			break;
	}
?>