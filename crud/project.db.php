<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
require_once CLASSPATH.'database.class.php';
/**
* 
*/
class Project extends Database{

	function getCollaborators($id_project){
		$sql = "SELECT * FROM user WHERE id_user IN (
							SELECT id_user FROM user_project WHERE id_project = :id_project AND 
								id_role IN (
										SELECT id_role FROM role WHERE role = 'collaborator'
									)
								)";
		$statement = $this->bd->prepare($sql);
		$statement->bindParam(":id_project",$id_project);
		$statement->Execute();
		$array=$statement->FetchAll(PDO::FETCH_ASSOC);
		return $array;
	}

	function addCollaborator($id_project,$username){
		$sql = "INSERT INTO user_project (id_project,id_user,id_role) 
			VALUES (:id_project,
					(SELECT id_user FROM user WHERE username=:username),
					(SELECT id_role FROM role WHERE role ='collaborator')
					)";
		$statement =$this->bd->prepare($sql);
		$statement->bindParam(":id_project",$id_project);
		$statement->bindParam(":username",$username);
		$statement->Execute();
	}
	/**
	 * Crea un registro de un project en la tabla project
	 * @param data: array con los datos del nuevo project
	**/
	function create($data,$header = false){
		$sql = "INSERT INTO project (id_creator,title,public,info,date_begin) values (:id_creator,:title,:public,:info,now())";
		$statement = $this -> bd -> prepare($sql);
			$statement->bindParam(':id_creator',$data['id_creator']);
			$statement->bindParam(':title',$data['title']);
			$statement->bindParam(':public',$data['public']);
			$statement->bindParam(':info',$data['info']);
			$statement -> Execute();
		if($header)
			$this->read(true);
		else
			return $statement;
	}


	function read($header = false){
		$data = array();
		$sql = 'SELECT * from project';
		$data = $this -> query($sql);
		if ($header)
			$this->render("project_read.php",$data);
		else
			return $data;
	}

	function update($id_project,$data,$header=false){
		$sql = "UPDATE project SET title=:title,public=:public,info=:info WHERE id_project=:id_project";
		$statement = $this->bd->prepare($sql);
			$statement->bindParam(":title",$data['title']); 
			$statement->bindParam(":public",$data['public']); 
			$statement->bindParam(":info",$data['info']); 
			$statement->bindParam(":id_project",$id_project); 
		$statement->Execute();
		if($header)
			$this->read(true);
		else
			return $statement;
	}

	function delete($id_project,$header=false){
		$sql = "DELETE FROM project WHERE id_project=:id_project";
		$statement = $this->bd->prepare($sql);
		$statement->bindParam(":id_project",$id_project);
		$statement->Execute();
		if($header)
			$this->read(true);
		else
			return $statement;
	}

	function projectRole($id_user,$id_project){
		$sql = "SELECT role FROM user_project JOIN role USING(id_role) WHERE id_user=:id_user AND id_project=:id_project";
		$statement = $this->bd->prepare($sql);
			$statement->bindParam(':id_user',$id_user);
			$statement->bindParam(':id_project',$id_project);
		$statement->Execute();
		$array = $statement->FetchAll(PDO::FETCH_ASSOC);
		if (count($array) > 0) {
			return $array[0]['role'];
		}else{
			return 'unset';
		}
	}

	/**
	 * [getID description]
	 * @param  [type] $id_user [description]
	 * @param  [type] $title   [description]
	 * @return [type]          [description]
	 */
	function getID($id_user,$title){
		$sql = "SELECT id_project FROM project WHERE id_creator=:id_user AND title=:title";
		$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_user", $id_user);
			$statement->bindParam(":title", $title);
		$statement->Execute();
		$array = $statement->FetchAll(PDO::FETCH_ASSOC); 
		return $array[0]['id_project'];
	}


	function readWhere($id_user){
		return $this->fetch('SELECT * FROM project WHERE id_creator!='.$id_user);
	}

	function readOne($id_project){
		$sql = "SELECT * from project WHERE id_project=:id_project";
		$statement=$this->bd->prepare($sql);
			$statement->bindParam(':id_project',$id_project);
		$statement->Execute();
		$array = $statement->FetchAll(PDO::FETCH_ASSOC);
		return $array[0];
	}

	function userProjects($id_user){
		return $this->fetch('SELECT * FROM project WHERE id_creator='.$id_user);
	}

	function files_detail($id_project){
		return $this->fetch('SELECT * FROM file WHERE id_project='.$id_project);	
	}

		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('project_form.php', $data, $action, $id);
		} 
}

	if (isset($_SESSION['crud'])){
		$web = new Project;
		$web->connectdb();
		$web->access('admin');

		$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : '';
		switch ($action) {
			case 'edit':
				$params['id_project']=$_GET['id_project'];
				$project=$web->fetchAll("SELECT * FROM project",$params);
				$data=$project[0];
				$web->form('update',$_GET['id_project'],$data);
			break;		
			case 'update':
					$web->update($_REQUEST['id_project'],$_REQUEST['data'],true);
				break;
			case 'create':
					$data = $_GET['data'];
					$web->create($data,true);
				break;
			case 'new':
					$web->form();
				break;
			case 'delete':
				if (isset($_GET['id_project'])) {
					$id_project = $_GET['id_project'];
					$web->delete($id_project);
					$web->read(true);
				}
			break;				
			case 'readOne':
				if(isset($_GET['id_project']))
					$web->readOne($_GET['id_project']);
				break;
			default:
				$web->read(true);
				break;
		}
	}
?>