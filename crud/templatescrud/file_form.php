<!DOCTYPE html>
<html>
<head>
	<title>Nuevo File</title>
</head>
<body>
	<form action="file.db.php" method="post" enctype="multipart/form-data">
		<div class="form-group">
			Selecciona archivo a subir
			<input type="file" name="fileToUpload" id="fileToUpload" class="form-control-file" required>	
		</div>
		<div class="form-group">
			<label for="name">Nombre del archivo</label>
			<input type="text" class="form-control datafield" name="name" required>
		</div>
		<div class="form-group">
			<label for="instrument">Instrumento</label>
			<select class="form-control" name="instrument" id="instrument">
			  {CONTENT}
			</select>
	    </div>
		<div class="form-group">
			Description
				<textarea class="form-control" rows="3" name="description"></textarea>			
		</div>
		<button type="submit" class="btn btn-primary"  name="submit" value="submit">Submit</button>
	</form>
</body>
</html>
