<!DOCTYPE html>
<html>
<head>
	<title>Nuevo usuario</title>
</head>
<body>
	<form action="user.db.php"  method="GET">
		<input type="hidden" name="action" value="<?php echo $action;?>">

		<?php 
			if (isset($id)) {
				echo '<input type="hidden" name="id_user" value='.$id.'">';
			}
		 ?>

		<div class="form-group">
	 		<label for="username">Nombre de usuario</label>
			<input type="text" class="form-control datafield" name="data[username]" <?php if (isset($data['username'])) {echo "value='".$data['username']."'";}?>
			required>
		</div>	
		<div class="form-group">
	 		<label for="name">Nombre</label>
			<input type="text" class="form-control datafield" name="data[name]" 
				<?php if (isset($data['name'])) {echo "value='".$data['name']."'";}?>
			required>
		</div>			
		<div class="form-group">
			<label for="lastname">Apellido</label>
			<input type="text" class="form-control datafield" name="data[lastname]"
				<?php if (isset($data['lastname'])) {echo "value='".$data['lastname']."'";}?>
			 required>
		</div>
		<div class="form-group">
		    <label for="email">Email</label>
		    <input type="email" class="form-control datafield" name="data[email]" 
		    	<?php if (isset($data['email'])) {echo "value='".$data['email']."'";}?> 
		    required>
		</div>					
		<div class="form-group">
		    <label for="password">Contraseña</label>
		    <input type="password" class="form-control datafield" name="data[password]" required>
		</div>
		<div class="form-group">
		    <label for="password">Confirmar contraseña</label>
		    <input type="password" class="form-control datafield" name="data[password_check]" required>
		</div>					
			<button type="submit" class="btn btn-primary" name="submit" value="register">Listo</button>
	</form>	
</body>
</html>
