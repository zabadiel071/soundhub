<!DOCTYPE html>
<html>
<head>
	<title>CRUD</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.2.0.min.js" integrity="
    sha256-JAW99MJVpJBGcbzEuXk4Az05s/XyDdBomFqNlM3ic+I=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
	<h1>Soundhub CRUD</h1>
	<a href="login.php?action=logout">Logout</a>
	<div class="container-fluid">
		<h3>Tablas</h3>
		<h4>Sistema de usuarios</h4>
		<hr>
		<ul class="list-group">
			<li class="list-group-item"><a href="user.db.php">USER</a></li>
			<li class="list-group-item"><a href="role.db.php">ROLE</a></li>
			<li class="list-group-item"><a href="role_type.db.php">ROLE_TYPE</a></li>
			<li class="list-group-item"><a href="permission.db.php">PERMISION</a></li>
		</ul>
		<h4>Proyectos</h4>
		<hr>
		<ul class="list-group">
			<li class="list-group-item"><a href="file.db.php">FILE</a></li>	
			<li class="list-group-item"><a href="instrument.db.php">INSTRUMENT</a></li>
			<li class="list-group-item"><a href="format.db.php">FORMAT</a></li>	
			<li class="list-group-item"><a href="project.db.php">PROJECT</a></li>	
			<li class="list-group-item"><a href="user_project.db.php">USER_PROJECT</a></li>	
		</ul>
		<h4>Mensajes</h4>
		<hr>
		<ul class="list-group">
			<li class="list-group-item"><a href="message.db.php">MESSAGE</a></li>	
		</ul>
	</div>
</body>
</html>
