<form method="GET" action="project.db.php">
<input type="hidden" name="action" value="<?php echo $action;?>">

		<?php 
			if (isset($id)) {
				echo '<input type="hidden" name="id_project" value='.$id.'>';
			}
		 ?>
 	<div class="form-group">
 		<label for="id_creator">ID Creador</label>
		<input type="text" class="form-control datafield" name="data[id_creator]" <?php if (isset($data['id_creator'])) {echo "value='".$data['id_creator']."'";}?> required>
	</div>
	<div class="form-group">
 		<label for="title">Titulo</label>
		<input type="text" class="form-control datafield" name="data[title]" <?php if (isset($data['title'])) {echo "value='".$data['title']."'";}?> required>
	</div>	
	<div class="form-group">
 		<label for="info">Descripción</label>
		<textarea class="form-control" name="data[info]" rows="5" style="resize: none;" required><?php if (isset($data['info'])) echo $data['info'];?></textarea>
	</div>	

	<div class="form-group">
		<label class="radio-inline">
			<input type="radio" name="data[public]" id="input1" required value="1" 
				<?php 
					if (isset($data['public'])) {
						if($data['public'] == 1)
						echo "checked";
					}
				?>
			>Publico 
		</label>
		<label class="radio-inline">
			<input type="radio" name="data[public]" id="input2" value="0"
				<?php 
					if (isset($data['public'])) {
						if($data['public'] == 0)
						echo "checked";
					}
				?>
			>Privado 
		</label>
	</div>
	<button type="submit" class="btn btn-primary" name="data[submit]" value="updateData">Listo</button>				
</form>		