<!DOCTYPE html>
<html>
<head>
	<title>Nuevo instrumento</title>
</head>
<body>
	<form action="instrument.db.php"  method="GET">
		<input type="hidden" name="action" value="<?php echo $action;?>">

		<?php 
			if (isset($id)) {
				echo '<input type="hidden" name="id_instrument" value='.$id.'>';
			}
		 ?>

		<div class="form-group">
	 		<label for="instrument">Nombre de tipo</label>
			<input type="text" class="form-control datafield" name="data[instrument]" <?php if (isset($data['instrument'])) {echo "value='".$data['instrument']."'";}?>
			required>
		</div>					
			<button type="submit" class="btn btn-primary" name="submit" value="register">Listo</button>
	</form>	
</body>
</html>
