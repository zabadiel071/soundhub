<!DOCTYPE html>
<html>
<head>
	<title>Nuevo usuario</title>
</head>
<body>
	<form action="permission.db.php"  method="GET">
		<input type="hidden" name="action" value="<?php echo $action;?>">

		<?php 
			if (isset($id)) {
				echo '<input type="hidden" name="id_permission" value='.$id.'>';
			}
		 ?>

		<div class="form-group">
	 		<label for="permission">Permiso</label>
			<input type="text" class="form-control datafield" name="data[permission]" <?php if (isset($data['permission'])) {echo "value='".$data['permission']."'";}?>
			required>
		</div>					
		<button type="submit" class="btn btn-primary" name="submit" value="register">Listo</button>
	</form>	
</body>
</html>
