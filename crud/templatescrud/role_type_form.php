<!DOCTYPE html>
<html>
<head>
	<title>Nuevo tipo de rol</title>
</head>
<body>
	<form action="role_type.db.php"  method="GET">
		<input type="hidden" name="action" value="<?php echo $action;?>">

		<?php 
			if (isset($id)) {
				echo '<input type="hidden" name="id_role_type" value='.$id.'>';
			}
		 ?>

		<div class="form-group">
	 		<label for="role_type">Nombre de tipo</label>
			<input type="text" class="form-control datafield" name="data[role_type]" <?php if (isset($data['role_type'])) {echo "value='".$data['role_type']."'";}?>
			required>
		</div>					
			<button type="submit" class="btn btn-primary" name="submit" value="register">Listo</button>
	</form>	
</body>
</html>
