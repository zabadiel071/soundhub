<?php 
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';
	require_once ROOT_DIR.'\crud\user.db.php';

 	class User_Role extends Database{
		
		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [description]
		 * @return [type]          [description]
		 */
		function create($data,$crud = false,$header = false){
			$sql = "INSERT INTO user_role(id_user,id_role) VALUES (:id_user,:id_role)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_user",$data['id_user']);
			$statement->bindParam(":id_role",$data['id_role']);
			$statement->Execute();
			header("Location: user.db.php?action=readOne&id_user=".$data['id_user']);
		}

		function namesCreate($username,$role){
			$sql = "INSERT INTO user_role(id_user,id_role) VALUES (
				(SELECT id_user FROM user WHERE username=:username),
				(SELECT id_role FROM role WHERE role=:role)
				)";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":username",$username);
			$statement->bindParam(":role",$role);
			$statement->Execute();
			return $statement;
		}

		function update($id_user_role,$data){
			$sql = "UPDATE user_role 
					SET id_user=(SELECT id_user FROM user WHERE username=:username),
						id_role=(SELECT id_role FROM role WHERE role=:role)
					WHERE id_user_role=:id_user_role";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":username",$data['username']);
			$statement->bindParam(":role",$data['role']);
			$statement->bindParam(":id_user_role",$id_user_role);
			$statement->Execute();
			return $statement;
		}

		function namesDelete($id_user_role){
			$sql = "DELETE FROM user_role
				WHERE id_user_role=:id_user_role";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_user_role",$id_user_role);
			$statement->Execute();
			return $statement;
		}

		function read(){
			$sql = "SELECT id_user_role,id_role,role,id_user,username FROM user_role JOIN role using (id_role) JOIN user using(id_user)";
			$data = $this -> query($sql);
			return $data;
		}


		function readOne($id){
			$sql = "SELECT id_user_role,id_role,role,id_user,username FROM user_role JOIN role using (id_role) JOIN user using(id_user) WHERE id_user_role=:id_user_role";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_user_role",$id);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);
		}

		function readSome($username){
			$sql = "SELECT id_user_role,id_role,role,id_user,username FROM user_role JOIN role using (id_role) JOIN user using(id_user) 
				WHERE id_user=(SELECT id_user FROM user WHERE username=:username)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":username",$username);
			$statement->Execute();
			return $statement->FetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_user,$id_role){
			$sql = "DELETE FROM user_role WHERE id_user=:id_user AND id_role=:id_role";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_user",$id_user);
			$statement->bindParam(":id_role",$id_role);
			$statement->Execute();
			header("Location: user.db.php?action=readOne&id_user=".$id_user);	
		}
		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('user_role_form.php', $data, $action, $id);
		} 
 	
	}
	
	if (isset($_SESSION['crud'])){
		$web = new User_Role;
		$web->connectdb();
		$web->access('admin');

		$action = (isset($_GET['action'])) ? $_GET['action'] : '';

		switch ($action) {
		case 'create':
			$data = array('id_user' => $_GET['id_user'],
						  'id_role' => $_GET['data']['id_role']);
			$web->create($data,true,true);
			break;
		case 'new':
			$id['id_user'] = $_GET['id_user'];
			$web->form('create',$id);
			break;
		case 'delete':
			$web->delete($_GET['id_user'],$_GET['id_role']);
			break;
		}
	}
	
?>