<?php 
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';
	/**
	* 
	*/
	class Message extends Database{

		function write($array){
			$sql = "INSERT INTO message(id_sender,id_receiver,body) VALUES (:id_sender,:id_receiver,:body)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_sender",$array['id_sender']);
			$statement->bindParam(":id_receiver",$array['id_receiver']);
			$statement->bindParam(":body",$array['body']);
			$statement->Execute();
		}

		function sent($id_user){
			return $this->fetch('SELECT rec.username,body FROM message JOIN user rec on rec.id_user=id_receiver WHERE id_sender='.$id_user);
		}

		function received($id_user){
			return $this->fetch('SELECT sender.username,body FROM message JOIN user sender on sender.id_user=id_sender WHERE id_receiver='.$id_user);
		}


		
		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [description]
		 * @return [type]          [description]
		 */
		function create($data,$header = false){
			$sql = "INSERT INTO message(id_sender,id_receiver,body) VALUES (:id_sender,:id_receiver,:body)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_sender",$data['id_sender']);
			$statement->bindParam(":id_receiver",$data['id_receiver']);
			$statement->bindParam(":body",$data['body']);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Obtiene un array con los datos de todos los users  
		 * @param header, true->carga el read(true);
		 * @return SELECT FROM users
		**/
		function read($header=false){
			$sql = 'SELECT * from message';
			$data = $this -> query($sql);
			if ($header)
				$this->render("message_read.php",$data);
			else
				return $data;
		}

		function readOne($id_message,$header = false){
			$sql = "SELECT * FROM message WHERE id_message=:id_message";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":id_message",$id_message);
			$statement->Execute();
			if(!$header)
				return $statement->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Actualiza el registro con el id_role
		 * @return filas actualizadas
		**/
		function update($id_message,$data,$header=false){
			$sql="UPDATE message 
				SET body=:body
					WHERE id_message=:id_message";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':body',$data['body']);
				$statement->bindParam(':id_message',$id_message);
			$statement->Execute(); 
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_message,$header=false){
			$sql = "DELETE FROM message WHERE id_message=:id_message";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_message",$id_message);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}
		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('message_form.php', $data, $action, $id);
		} 
	}

	$action=null;
	if (isset($_GET['action'])) {
		$action=$_GET['action'];
	}
	if (isset($_SESSION['crud'])){
		$web = new Message;
		$web->access('admin');
		$web->connectdb();	
		switch ($action) {
			case 'edit':
					$params['id_format']=$_GET['id_format'];
					$format=$web->fetchAll("SELECT * FROM format",$params);
					$data=$format[0];
					$web->form('update',$_GET['id_format'],$data);
				break;		
			case 'update':
					$web->update($_GET['id_format'],$_GET['data'],true);
				break;
			case 'create':
				$data = $_GET['data'];
				$web->create($data,true,true);
				break;
			case 'new':
				$web->form();
				break;
			case 'delete':
				$web->delete($_GET['id_message'],true);
				break;
			default:
				$web->read(true);
				break;
		}
	}

?>