<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';

 	class Format extends Database{
		
		/**
		 * [create description]
		 * @param  [type]  $data   [description]
		 * @param  boolean $header [description]
		 * @return [type]          [description]
		 */
		function create($data,$header=false){
			$sql = "INSERT INTO format(format) VALUES (:format)";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":format",$data['format']);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Obtiene un array con los datos de todos los users  
		 * @param show, true->carga el read(true);
		 * @return SELECT FROM users
		**/
		function read($header=false){
			$format = array();
			$sql = 'SELECT id_format,format from format';
			$format = $this -> query($sql);
			if ($header)
				$this->render("format_read.php",$format);
			else
				return $format;
		}

		function readOne($id,$header=false){
			$sql = 'SELECT * from format WHERE id_format=:id_format';
			$statement = $this -> bd -> prepare($sql);
			$statement->bindParam(':id_format',$id);
			$statement -> Execute();
			if (!$header) 
				return $statement->FetchAll(PDO::FETCH_ASSOC);;
		}



		/**
		 * Actualiza el registro con el id_role
		 * @return filas actualizadas
		**/
		function update($id_format,$data,$header=false){
			$sql="UPDATE format 
				SET format=:format
					WHERE id_format=:id_format";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':format',$data['format']);
				$statement->bindParam(':id_format',$id_format);
			$statement->Execute(); 
			if($header)
				$this->read(true);
			else
				return $statement;
		}

		/**
		 * Borra un registro de la tabla
		 * @param id_user : identificador del user a borrar
		**/
		function delete($id_format,$header=false){
			$sql = "DELETE FROM format WHERE id_format=:id_format";
			$statement=$this->bd->prepare($sql);
			$statement->bindParam(":id_format",$id_format);
			$statement->Execute();
			if($header)
				$this->read(true);
			else
				return $statement;
		}
		/**
		 *	Mostrar el formulario
		 *	@param 		int			id 
		 *	@param 		array 		data
		 *	@param 		action 		string
		**/
		function form($action='create',$id=null,$data=null){
			$this->render('format_form.php', $data, $action, $id);
		} 
 	
	}


	$action=null;
	if (isset($_GET['action'])) {
		$action=$_GET['action'];
	}
	if (isset($_SESSION['crud'])){
		$web = new Format;
		$web->access('admin');
		$web->connectdb();
		switch ($action) {
			case 'edit':
					$params['id_format']=$_GET['id_format'];
					$format=$web->fetchAll("SELECT * FROM format",$params);
					$data=$format[0];
					$web->form('update',$_GET['id_format'],$data);
				break;		
			case 'update':
					$web->update($_GET['id_format'],$_GET['data'],true);
				break;
			case 'create':
				$data = $_GET['data'];
				$web->create($data,true);
				break;
			case 'new':
				$web->form();
				break;
			case 'delete':
				$web->delete($_GET['id_format'],true);
				break;
			default:
				$web->read(true);
				break;
		}
	}

  ?>