<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/soundhub/config.php';
	require_once CLASSPATH.'database.class.php';
	/**
	* 
	*/
	class File extends Database{

		/**
		 * [create description]
		 * @param  [type] $id_project [description]
		 * @param  [type] $data       [description]
		 * @return [type]             [description]
		 */	
		function create($data){
			$sql = "INSERT INTO file (id_project,id_format,name,source,id_instrument,date_upload,description) values (:id_project,:id_format,:name,:source,:id_instrument,now(),:description)";
			$statement = $this -> bd -> prepare($sql);
				$statement->bindParam(':id_project',$data['id_project']);
				$statement->bindParam(':id_format',$data['id_format']);
				$statement->bindParam(':name',$data['name']);
				$statement->bindParam(':source',$data['source']);
				$statement->bindParam(':id_instrument',$data['id_instrument']);
				$statement->bindParam(':description',$data['description']);
				$statement -> Execute();
		}

		function delete($id_file){
			$sql = "DELETE FROM file WHERE id_file=:id_file";
			$statement = $this->bd->prepare($sql);
				$statement->bindParam(':id_file',$id_file);
				$statement->Execute();
		}

		/**
		 * [update description]
		 * @param  [type] $data [description]
		 * @return [type]       [description]
		 */
		function update($data){

			$oldData = $this->readOne($data['id_file']);
			$oldSource = $oldData['source'];

			$dir = ROOT_DIR."\uploads\\";
			$newSource  = $dir.basename("project_".$data['id_project'].$data['name']);
			$sql = 'UPDATE file SET name=:name, description=:description,source=:source WHERE id_file=:id_file';
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(":name",$data['name']);
			$statement->bindParam(":description",$data['description']);
			$statement->bindParam(":id_file",$data['id_file']);
			$statement->bindParam(":source",$newSource);
			$statement->Execute();

			rename($oldSource, $newSource);
		}

		function instruments(){
			$sql = "SELECT id_instrument,instrument FROM instrument";
			return $this->fetch($sql);
		}

		function getFormat($format){
			$sql = "SELECT id_format FROM format WHERE format=:format";
			$statement = $this->bd -> prepare($sql);
				$statement->bindParam(":format",$format);
			$statement->Execute();
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			if (isset($result[0]['id_format'])) {
				return $result[0]['id_format'];
			}
			else {
				return "error";
			}
		}

		function list($id_project = null){
			return $this->fetch('SELECT * FROM file WHERE id_project='.$id_project);
		}


		function register($id_file){
			$sql = "SELECT * FROM file WHERE id_file=:id_file";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(':id_file',$id_file);
			$statement->Execute();
			return $statement->fetch(PDO::FETCH_ASSOC);
		}

		function read($show = true){
		$file = array();
		$sql = 'SELECT * from file';
		$file = $this -> query($sql);
		if ($show)
			$this->render("file_read.php",$file);
		else
			return $file;
		}

		function readOne($id_file){
			$sql = "SELECT * FROM file WHERE id_file=:id_file";
			$statement = $this->bd->prepare($sql);
			$statement->bindParam(':id_file',$id_file);
			$statement->Execute();
			$array = $statement->fetchAll(PDO::FETCH_ASSOC);
			return $array[0];
		}

		function form($action='create',$id=null,$data=null){
			$this->render("file_form.php",$file);
		}
	}


	if (isset($_GET['action'])) {
		$action=$_GET['action'];
	}
	if (isset($_SESSION['crud'])){
			$web = new File;
		$web->access('admin');
		$web->connectdb();
		$action=null;
		switch ($action) {
			case 'edit':
					$params['id_format']=$_GET['id_format'];
					$format=$web->fetchAll("SELECT * FROM format",$params);
					$data=$format[0];
					$web->form('update',$_GET['id_format'],$data);
				break;		
			case 'update':
					$web->update($_REQUEST);
				break;
			case 'create':
				$data = $_GET['data'];
				$web->create($data,true,true);
				break;
			case 'new':
				$web->form();
				break;
			case 'delete':
				$web->delete($_GET['id_format']);
				break;
			default:
				$web->read();
				break;
		}
	}
?>